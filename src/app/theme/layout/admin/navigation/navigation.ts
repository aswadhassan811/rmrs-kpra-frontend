import { Injectable } from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

const NavigationItems = [
  {
    id: 'navigation',
    title: 'Navigation',
    type: 'group',
    icon: 'feather icon-monitor',
    children: [
      {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'item',
        url: '/dashboard/analytics',
        icon: 'feather icon-home',
        breadcrumbs: false
      },
      

      // {
      //   id: 'page-layouts',
      //   title: 'Page Layouts',
      //   type: 'collapse',
      //   icon: 'feather icon-layout',
      //   children: [
      //     {
      //       id: 'vertical',
      //       title: 'Vertical',
      //       type: 'item',
      //       url: '/layout/static',
      //       target: true
      //     },
      //     {
      //       id: 'horizontal',
      //       title: 'Horizontal',
      //       type: 'item',
      //       url: '/layout/horizontal',
      //       target: true
      //     }
      //   ]
      // }
    ]
  },
  {
    id: 'staff-managment',
    title: 'Managment',
    type: 'group',
    icon: 'feather icon-layers',
    children: [
      {
        id: 'restaurant',
        title: 'Restaurant',
        type: 'collapse',
        icon: 'feather icon-box',
        children: [
          {
            id: 'restaurant-add',
            title: 'Add restaurant',
            type: 'item',
            url: '/restaurant/restaurant-add',
          },
          {
            id: 'restaurant-list',
            title: 'List of restaurant',
            type: 'item',
            url: '/restaurant/restaurant-list',
          }
        ]
      },
      {
        id: 'staff',
        title: 'Staff',
        type: 'collapse',
        icon: 'feather icon-users',
        children: [
          {
            id: 'staff-add',
            title: 'Add Staff',
            type: 'item',
            url: '/staff/staff-add',
          },
          {
            id: 'staff-list',
            title: 'List of Staff',
            type: 'item',
            url: '/staff/staff-list',
          }
        ]
      },
      {
        id: 'regions',
        title: 'Regions',
        type: 'collapse',
        icon: 'feather icon-users',
        children: [
          {
            id: 'add-regions',
            title: 'Add Regions',
            type: 'item',
            url: '/regions/add-regions',
            breadcrumbs: false
          },
          {
            id: 'add-divisions',
            title: 'Add Division',
            type: 'item',
            url: '/regions/add-divisions',
            breadcrumbs: false
          },
          {
            id: 'add-districts',
            title: 'Add Districts',
            type: 'item',
            url: '/regions/add-districts',
            breadcrumbs: false
          },
          {
            id: 'add-cities',
            title: 'Add Cities',
            type: 'item',
            url: '/regions/add-cities',
            breadcrumbs: false
          },
          {
            id: 'location-config',
            title: 'Location Config',
            type: 'item',
            url: '/regions/location-config',
            breadcrumbs: false
          },
          // {
          //   id: 'staff-list',
          //   title: 'List of Staff',
          //   type: 'item',
          //   url: '/staff/staff-list',
          // }
        ]
      },
      {
        id: 'invoice',
        title: 'Invoice',
        type: 'collapse',
        icon: 'feather icon-users',
        children: [
          {
            id: 'invoices',
            title: 'Invoices',
            type: 'item',
            url: '/invoice/invoices',
          },
        ]
      }
    ]
  },

  // {
  //   id: 'ui-element',
  //   title: 'UI ELEMENT',
  //   type: 'group',
  //   icon: 'feather icon-layers',
  //   children: [
  //     {
  //       id: 'basic',
  //       title: 'Basic',
  //       type: 'collapse',
  //       icon: 'feather icon-box',
  //       children: [
  //         {
  //           id: 'alert',
  //           title: 'Alert',
  //           type: 'item',
  //           url: '/basic/alert'
  //         },
  //         {
  //           id: 'button',
  //           title: 'Button',
  //           type: 'item',
  //           url: '/basic/button'
  //         },
  //         {
  //           id: 'badges',
  //           title: 'Badges',
  //           type: 'item',
  //           url: '/basic/badges'
  //         },
  //         {
  //           id: 'breadcrumb-pagination',
  //           title: 'Breadcrumbs & Pagination',
  //           type: 'item',
  //           url: '/basic/breadcrumb-paging'
  //         },
  //         {
  //           id: 'cards',
  //           title: 'Cards',
  //           type: 'item',
  //           url: '/basic/cards'
  //         },
  //         {
  //           id: 'collapse',
  //           title: 'Collapse',
  //           type: 'item',
  //           url: '/basic/collapse'
  //         },
  //         {
  //           id: 'carousel',
  //           title: 'Carousel',
  //           type: 'item',
  //           url: '/basic/carousel'
  //         },
  //         {
  //           id: 'grid-system',
  //           title: 'Grid System',
  //           type: 'item',
  //           url: '/basic/grid-system'
  //         },
  //         {
  //           id: 'progress',
  //           title: 'Progress',
  //           type: 'item',
  //           url: '/basic/progress'
  //         },
  //         {
  //           id: 'modal',
  //           title: 'Modal',
  //           type: 'item',
  //           url: '/basic/modal'
  //         },
  //         {
  //           id: 'spinner',
  //           title: 'Spinner',
  //           type: 'item',
  //           url: '/basic/spinner'
  //         },
  //         {
  //           id: 'tabs-pills',
  //           title: 'Tabs & Pills',
  //           type: 'item',
  //           url: '/basic/tabs-pills'
  //         },
  //         {
  //           id: 'typography',
  //           title: 'Typography',
  //           type: 'item',
  //           url: '/basic/typography'
  //         },
  //         {
  //           id: 'tooltip-popovers',
  //           title: 'Tooltip & Popovers',
  //           type: 'item',
  //           url: '/basic/tooltip-popovers'
  //         },
  //         {
  //           id: 'toasts',
  //           title: 'Toasts',
  //           type: 'item',
  //           url: '/basic/toasts'
  //         },
  //         {
  //           id: 'other',
  //           title: 'Other',
  //           type: 'item',
  //           url: '/basic/other'
  //         }
  //       ]
  //     }
  //   ]
  // },
  {
    id: 'Reporting',
    title: 'Reporting',
    type: 'group',
    icon: 'feather icon-layers',
    children: [
      {
        id: 'reports',
        title: 'Reports',
        type: 'collapse',
        icon: 'feather icon-box',
        children: [
          {
            id: 'sales-report',
            title: 'Sales Report',
            type: 'item',
            url: '/reports/sales-report',
          },
          {
            id: 'tax-collection-report',
            title: 'Tax Collection Report',
            type: 'item',
            url: '/reports/tax-collection-report',
          },
          {
            id: 'comparison-sales-report',
            title: 'Comparison Sales Report',
            type: 'item',
            url: '/reports/comparison-sales-report',
          },
          {
            id: 'suspected-discrepancy-reports',
            title: 'Suspected Discrepancy Reports',
            type: 'item',
            url: '/reports/suspected-discrepancy-reports',
          },
          {
            id: 'status-report',
            title: 'Status Report',
            type: 'item',
            url: '/reports/status-report',
          }
        ]
      },
      {
        id: 'lucky-draw-scheme',
        title: 'Lucky draw scheme',
        type: 'collapse',
        icon: 'feather icon-users',
        children: [
          {
            id: 'lucky-draw',
            title: 'Lucky Draw',
            type: 'item',
            url: '/lucky-draw-scheme/lucky-draw',
          },
        ]
      },
      // {
      //   id: 'dashboard',
      //   title: 'Lucky Draw',
      //   type: 'item',
      //   url: '/lucky-draw',
      //   icon: 'feather icon-home',
      //   // breadcrumbs: false
      // }
      

    ]
  },
  // {
  //   id: 'chart-maps',
  //   title: 'Chart & Maps',
  //   type: 'group',
  //   icon: 'feather icon-pie-chart',
  //   children: [
  //     {
  //       id: 'charts',
  //       title: 'Charts',
  //       type: 'item',
  //       url: '/charts/apex',
  //       icon: 'feather icon-pie-chart'
  //     },
  //     {
  //       id: 'maps',
  //       title: 'Maps',
  //       type: 'item',
  //       url: '/maps/google',
  //       icon: 'feather icon-map'
  //     }
  //   ]
  // },
  // {
  //   id: 'pages',
  //   title: 'Pages',
  //   type: 'group',
  //   icon: 'feather icon-file-text',
  // children: [
  // {
  //   id: 'auth',
  //   title: 'Authentication',
  //   type: 'collapse',
  //   icon: 'feather icon-lock',
  // children: [
  // {
  //   id: 'signup',
  //   title: 'Sign up',
  //   type: 'item',
  //   url: '/auth/signup',
  //  // target: true,
  //   breadcrumbs: false
  // },
  // {
  //   id: 'signin',
  //   title: 'Sign in',
  //   type: 'item',
  //   url: '/auth/signin',
  //   //target: true,
  //   breadcrumbs: false
  // }
  // ]
  // }
  // {
  //   id: 'sample-page',
  //   title: 'Sample Page',
  //   type: 'item',
  //   url: '/sample-page',
  //   classes: 'nav-item',
  //   icon: 'feather icon-sidebar'
  // }
  // 
  // ]
  // }
];

@Injectable()
export class NavigationItem {
  public get() {
    return NavigationItems;
  }
}
