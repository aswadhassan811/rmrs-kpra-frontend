import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './theme/layout/admin/admin.component';
import {AuthComponent} from './theme/layout/auth/auth.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: '/auth/signin',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./demo/dashboard/dashboard.module').then(module => module.DashboardModule)
      },
      {
        path: 'layout',
        loadChildren: () => import('./demo/pages/layout/layout.module').then(module => module.LayoutModule)
      },
      {
        path: 'basic',
        loadChildren: () => import('./demo/ui-elements/ui-basic/ui-basic.module').then(module => module.UiBasicModule)
      },
      {
        path: 'forms',
        loadChildren: () => import('./demo/pages/form-elements/form-elements.module').then(module => module.FormElementsModule)
      },
      {
        path: 'tbl-bootstrap',
        loadChildren: () => import('./demo/pages/tables/tbl-bootstrap/tbl-bootstrap.module').then(module => module.TblBootstrapModule)
      },
      {
        path: 'charts',
        loadChildren: () => import('./demo/pages/core-chart/core-chart.module').then(module => module.CoreChartModule)
      },
      {
        path: 'maps',
        loadChildren: () => import('./demo/pages/core-maps/core-maps.module').then(module => module.CoreMapsModule)
      },
      {
        path: 'sample-page',
        loadChildren: () => import('./demo/pages/sample-page/sample-page.module').then(module => module.SamplePageModule)
      },
      {
        path: 'staff',
        loadChildren: () => import('./demo/pages/staff/staff.module').then(module => module.StaffModule)
      },
      {
        path: 'restaurant',
        loadChildren: () => import('./demo/pages/restaurant/restaurant.module').then(module => module.RestaurantModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('./demo/pages/profile/profile.module').then(module => module.ProfileModule)
      },
      {
        path: 'owner',
        loadChildren: () => import('./demo/pages/owner/owner.module').then(module => module.OwnerModule)
      },
      {
        path: 'regions',
        loadChildren: () => import('./demo/pages/regions/regions.module').then(module => module.RegionsModule)
      },
      {
        path: 'invoice',
        loadChildren: () => import('./demo/pages/invoice/invoice.module').then(module => module.InvoiceModule)
      },
      {
        path: 'reports',
        loadChildren: () => import('./demo/pages/reports/reports.module').then(module => module.ReportsModule)
      },
      {
        path: 'lucky-draw-scheme',
        loadChildren: () => import('./demo/pages/lucky-draw-scheme/lucky-draw-scheme.module').then(module => module.LuckyDrawSchemeModule)
      },
    ]
  },
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'auth',
        loadChildren: () => import('./demo/pages/authentication/authentication.module').then(module => module.AuthenticationModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
