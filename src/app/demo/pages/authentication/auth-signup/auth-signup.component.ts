import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';

@Component({
  selector: 'app-auth-signup',
  templateUrl: './auth-signup.component.html',
  styleUrls: ['./auth-signup.component.scss']
})
export class AuthSignupComponent implements OnInit {
  signUpForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  message: string;
  constructor(private formBuilder: FormBuilder,
    private router: Router, private restaurantService: RestaurantService) { }

  ngOnInit() {
    this.signUpForm = this.formBuilder.group({
      FullName: new FormControl('', [
        Validators.required]),
      DOB: new FormControl('', [
        Validators.required]),
      Gender: new FormControl('', [
        Validators.required]),
      CNICNo: new FormControl('', [
        Validators.required,
        Validators.pattern("^[0-9]{13}$")]),
      Nationality: new FormControl('', []),
      Email: new FormControl('', [
        Validators.required,
        Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      Address: new FormControl('', [
        Validators.required]),
      CreatePassword: new FormControl('', [
        Validators.required, Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')]),
      PhoneNo: new FormControl('', [
        Validators.required,
        Validators.pattern(".{9,9}")]),
      MobileNo: new FormControl('', [
        Validators.required,
        Validators.pattern(".{10,10}")]),
      Checkbox: new FormControl('', [
        Validators.required]),

    }

    );
  }

  onSubmit() {

    this.signUpForm.markAllAsTouched()
    this.submitted = true;

    let verifyEmailBody = {
      "Email": this.signUpForm.value.Email,
      "PhoneNumber": this.signUpForm.value.PhoneNo
    }


    // let signUpFormValue = {
    //   Name: this.signUpForm.value.Name,
    //   Email: this.signUpForm.value.Email,
    //   Password: this.signUpForm.value.password,
    //   ConfirmPassword: this.signUpForm.value.confirm_password,
    //   CNIC: this.signUpForm.value.CNIC,
    //   Address: this.signUpForm.value.Address,
    //   PhoneNumber: this.signUpForm.value.PhoneNumber,
    //   MobileNumber: this.signUpForm.value.MobileNo

    // }


    // let signUpFormValue = {
    //   Name: this.user.Name,
    //   Email: this.user.Email,
    //   Password: this.user.Password,
    //   CNIC: this.user.CNIC,
    //   Address: this.user.Address,
    //   MobileNumber: this.user.MobileNumber,
    //   PhoneNumber: this.user.PhoneNumber
    // };


    this.restaurantService.verifyEmail(verifyEmailBody).subscribe(
      (data) => {
        //when email and phone number are not registrot sucess message is returned
        //Then call otp send api
        if (data.Message == 'Success') {
          // this.sessionService.signUpFormData = signUpFormValue;
          this.sendOTP(verifyEmailBody);

        }
        console.log('Email responce', data);

      },

      err => {
        console.log("Error  ", err);
        this.error = err.error.data.description;
        // this.loading = false;

      }
    );

  }

  sendOTP(verifyEmailBody: any) {
    this.restaurantService.sendOTP(verifyEmailBody).subscribe(
      (data) => {
        //when email and phone number are not registrot sucess message is returned
        //Then call otp send api
        if (data.Message == 'Success') {
            this.router.navigate(['/auth/otp']);
        }

      },
      err => {
        console.log("Error  ", err);
        // this.error = err.data.description;
        // console.log(err.data.description);
        // this.loading = false;

      }


    );
  }

}

