import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-auth-signin',
  templateUrl: './auth-signin.component.html',
  styleUrls: ['./auth-signin.component.scss']
})
export class AuthSigninComponent implements OnInit {
  signInForm: FormGroup;
  returnUrl: string;
  error = '';
  loading = false;
  Token: string = "";
  CustomerID: string = "";
  CustomerEmail: string;
  verifySigned: boolean;

  constructor(private fb: FormBuilder,
    private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.signInForm = this.fb.group({
      Email: new FormControl('', [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      Password: new FormControl('', [
        Validators.required]),
    });
  }

  onSubmit() {
    debugger;
    let body =
    {
      Email: this.signInForm.controls['Email'].value,
      Password: this.signInForm.controls['Password'].value
    }
    debugger;
    this.authService.AuthService_Login(body).subscribe(
      data => {
        debugger;
        console.log("Login data : ", data)
        // let accessToken = data.data.Token;
        // this.Token = data.data.Token;
        // this.CustomerID = data.data.CustomerID;
        // this.authService.getCustomerID(this.CustomerID);
        // localStorage.setItem("accessToken", accessToken);
        // this.router.navigate([this.returnUrl]);
        this.router.navigate(['/dashboard/analytics']);


      },
      err => {
        debugger;
        console.log("Error  ", err);
        this.error = err.error.data.description;
        this.loading = false;

      }
    );



  }

}