import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/shared/services/menu.service';
import { FormGroup, Validators } from '@angular/forms';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
@Component({
  selector: 'app-restaurant-add',
  templateUrl: './restaurant-add.component.html',
  styleUrls: ['./restaurant-add.component.scss']
})
export class RestaurantAddComponent implements OnInit {
  dropdownSettings: IDropdownSettings;
  restaurantAddForm: FormGroup;
  cancel: boolean;
  ADDorEDIT: boolean;//add or edit restaurant mode
  ownerID: any;
  restaurantID: any;
  error: any;
  loading: boolean;
  Restaurant: any;
  RestaurantID: any;
  RestaurantName: any;
  Address: any;
  OwnerId: any;
  NTNNumber: any;
  ResCategoryName: any;
  TotalDevices: any;
  RestaurantVerified: any;
  RestaurantType: any;
  RestaurantId: any;
  CategoryList: any;
  boo: boolean = false;
  DisplayMessage: boolean = false;
  ResCategoryId: any;
  Categoryid: any;
  Mode: boolean = false;
  Restaurantcategory: any;
  CityList: any;

  constructor(private formBuilder: FormBuilder,
    private menuService: MenuService,
    private router: Router,
    private restaurantService: RestaurantService,
    private authService: AuthService) { }
  dropdownList = [];
  selectedItems = [];
  ngOnInit(): void {
    this.restaurantAddForm = this.formBuilder.group({
      RestaurantName: new FormControl('', [
        Validators.required]),
      Address: new FormControl('', [
        Validators.required,]),
      NTN: new FormControl('', [
        Validators.required,
        Validators.pattern(".{9,9}")]),
      RestaurantCategoryInfo: new FormControl('', [
        Validators.required,]),
      RestaurantVerified: new FormControl('False', [
        Validators.required,]),
      CityName: new FormControl('False', [
        Validators.required,]),
      // TotalDevices: new FormControl('', [
      //   Validators.required,
      //   Validators.pattern(".{1,2}")]),
      categoryName: new FormControl('', [])
    }
    );
    this.OwnerId = localStorage.getItem("OwnerID");
    // this.restaurantService.updateRestaurantList(this.restaurantAddForm);
    // this.restaurantService.preFilledRestaurantEdit(this.restaurantAddForm);
    // this.menuService.updateRestaurantList(this.restaurantAddForm);
    //this.ownerID = this.restaurantService.ownerID;
    // this.ADDorEDIT = this.menuService.decide;
    debugger;
    // this.Mode = this.restaurantService.Mode;
    this.getCategory();
    this.getCityList();
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    }
    if (this.Mode) {
      this.restaurantAddForm.controls['RestaurantName'].patchValue(this.restaurantService.RestaurantEdit.RestaurantName);
      this.restaurantAddForm.controls['Address'].patchValue(this.restaurantService.RestaurantEdit.Address);
      this.restaurantAddForm.controls['NTN'].patchValue(this.restaurantService.RestaurantEdit.NTNNumber);
      this.restaurantAddForm.controls['RestaurantCategoryInfo'].patchValue(this.ResCategoryName);
      this.restaurantAddForm.controls['RestaurantVerified'].patchValue(this.restaurantService.RestaurantEdit.RestaurantVerified);
      // this.restaurantAddForm.controls['TotalDevices'].patchValue(this.restaurantService.RestaurantEdit.TotalDevices);

      debugger;
    }
    // this.Mode = false;
    //  this.restaurantService.editornot(this.Mode);
  }

  getRestaurantCategory(RestaurantCategory: any) {
    debugger;
    this.ResCategoryId = RestaurantCategory;
    this.ResCategoryId = this.ResCategoryId.slice(0, 10);
    debugger;
    this.ResCategoryName = RestaurantCategory;
    this.ResCategoryName = this.ResCategoryName.slice(10, 50);
    // this.restaurantService.RestuarantIDForStaffList(this.ResCategoryId);
  }
  getCategory() {
    this.restaurantService.getCategoryName().subscribe(
      data => {
        debugger;
        this.CategoryList = data['data'];
        console.log(this.CategoryList);
      },
      err => {
        debugger;
        console.log(err);

      }
    );
  }

  addCategory() {
    this.boo = true;
    let body = {
      CategoryName: this.restaurantAddForm.value.categoryName
    }
    this.restaurantService.addCategory(body).subscribe(
      data => {
        this.DisplayMessage = true;
        this.getCategory();
        this.ngOnInit();
      },
      err => {
        debugger;
        this.DisplayMessage = false;

      }
    );
  }
  onSubmit() {
    this.cancel = true;
    this.menuService.cancelF(this.cancel);

    let body =
    {
      RestaurantName: this.restaurantAddForm.controls['RestaurantName'].value,
      Address: this.restaurantAddForm.controls['Address'].value,
      NTNNumber: this.restaurantAddForm.controls['NTN'].value,
      RestaurantCategory: this.ResCategoryName,
      RestaurantVerified: this.restaurantAddForm.controls['RestaurantVerified'].value,
      CityName: this.restaurantAddForm.controls['CityName'].value[0],
      CategoryID: this.ResCategoryId
      //CategoryID: this.CategoryList.CategoryID
    }
    console.log("body: ", body)
    debugger;
    this.restaurantService.addRestaurant(body).subscribe(
      data => {
        this.RestaurantId = data.data.RestaurantID;
        localStorage.setItem("RestaurantID", this.RestaurantId);
        debugger;
        this.RestaurantID = data.data.RestaurantID;
        this.restaurantService.updateRestaurantID(this.RestaurantId);
        debugger;
        this.restaurantService.updateRestaurantList(this.restaurantAddForm);

        //  this.restaurantService.AllRestaurants(this.OwnerId);
        this.router.navigate(['/restaurant/restaurant-list']);


      },
      err => {
        debugger;
        console.log("Error  ", err);
        this.error = err.error.data.description;
        this.loading = false;

      });


  }
  canceled() {
    this.ngOnInit();
    this.router.navigate(['/restaurant/restaurant-list']);
  }

  getCityList() {
    this.restaurantService.getCity().subscribe(
      data => {
        //console.log(data['data'])
        let cities_data = []
        for (let i = 0; i < data['data'].length; i++) {
          // console.log(data['data'])
          cities_data.push({ item_id: data['data'][i].CityID, item_text: data['data'][i].CityName })
        }
        this.CityList = cities_data;
        console.log("Get City List: ", this.CityList);
        // Calling the DT trigger to manually render the table
        // this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
}