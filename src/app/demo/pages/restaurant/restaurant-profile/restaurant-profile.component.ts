import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/shared/services/menu.service';

@Component({
  selector: 'app-restaurant-profile',
  templateUrl: './restaurant-profile.component.html',
  styleUrls: ['./restaurant-profile.component.scss']
})
export class RestaurantProfileComponent implements OnInit {
  Restaurant: any;
  mode: any;
  constructor(private menuService: MenuService, private router: Router) { }

  ngOnInit(): void {
    this.Restaurant = this.menuService.restaurantFields;
  }

  editRestaurant() {
    this.mode = true;
    this.menuService.decision(this.mode);
    debugger;
    let restaurantInfo = {
      nameOfRestaurant: this.menuService.restaurantFields.value.NameOfRestaurant,
      address: this.menuService.restaurantFields.value.Address,
      kntn: this.menuService.restaurantFields.value.KNTN,
      weekdaysOpenTime: this.menuService.restaurantFields.value.WeekdaysOpenTime,
      weekdaysCloseTime: this.menuService.restaurantFields.value.WeekdaysCloseTime,
      weekendOpenTime: this.menuService.restaurantFields.value.WeekendOpenTime,
      weekendCloseTime: this.menuService.restaurantFields.value.WeekendCloseTime,

    }
    debugger;
    this.menuService.preFilledRestaurant(restaurantInfo);
    this.router.navigate(['/restaurant/restaurant-add']);
  }
}
