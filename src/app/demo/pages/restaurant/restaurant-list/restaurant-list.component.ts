import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/shared/services/menu.service';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.scss']
})
export class RestaurantListComponent implements OnInit {
  Restaurant: any;
  cancel: boolean;
  abc: boolean = false;
  RestaurantID: any;
  RestaurantList: any;
  Address: any;
  OwnerID: any;
  NTNNumber: any;
  RestaurantCategory: any;
  TotalDevices: any;
  RestaurantVerified: any;
  RestaurantType: any;
  CategoryID: any;
  error: any;
  loading: boolean;
  OwnerId: any;
  restriction: boolean = false;
  foo: any;
  constructor(private menuService: MenuService, private router: Router, private restaurantService: RestaurantService) { }

  ngOnInit(): void {
    
    this.getRestaurantList()
  }

  getRestaurantList(){
    this.Restaurant = this.restaurantService.signUpForm;
    this.cancel = this.menuService.cancelD;
    this.restaurantService.AllRestaurants(body).subscribe(
      data => {
        debugger;
        console.log(data);
         this.RestaurantList = data['data'];
        //this.restaurantService.addRestaurantListinStaff(this.RestaurantList);
        debugger;
      },
      err => {
        debugger;
        console.log("Error  ", err);
        this.error = err.error.data.description;
        this.loading = false;

      }
    );
    debugger;
  }
  addRestaurant() {
    this.menuService.decision(this.abc);
    this.router.navigate(['/restaurant/restaurant-add']);
  }

}
function body(body: any) {
  throw new Error('Function not implemented.');
}

