import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestaurantRoutingModule } from './restaurant-routing.module';
import { RestaurantAddComponent } from './restaurant-add/restaurant-add.component';
import { RestaurantListComponent } from './restaurant-list/restaurant-list.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { RestaurantProfileComponent } from './restaurant-profile/restaurant-profile.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';


@NgModule({
  declarations: [RestaurantAddComponent, RestaurantListComponent],
  imports: [
    CommonModule,
    RestaurantRoutingModule,
    SharedModule,
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class RestaurantModule { }
