import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegionsRoutingModule } from './regions-routing.module';
import { AddRegionsComponent } from './add-regions/add-regions.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { AddDistrictsComponent } from './add-districts/add-districts.component';
import { AddCitiesComponent } from './add-cities/add-cities.component';
import { AddDivisionsComponent } from './add-divisions/add-divisions.component';
import { DataTablesModule } from 'angular-datatables';
import { LocationConfigComponent } from './location-config/location-config.component';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [
    AddRegionsComponent,
    AddDistrictsComponent,
    AddCitiesComponent,
    AddDivisionsComponent,
    LocationConfigComponent
  ],
  imports: [
    CommonModule,
    RegionsRoutingModule,
    SharedModule,
    DataTablesModule,
    NgbTabsetModule,
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class RegionsModule { }
