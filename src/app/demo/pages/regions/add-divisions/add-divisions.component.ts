import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-add-divisions',
  templateUrl: './add-divisions.component.html',
  styleUrls: ['./add-divisions.component.scss']
})
export class AddDivisionsComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  fileToUpload: File = null;
  divisionAddForm: FormGroup;
  Location: any;
  DivisionList: any;
  getDivisionsList: any;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  Divisions: any;
  divisionsAddCSV: FormGroup;
  divisionCSV: any;
  constructor(private formBuilder: FormBuilder, private router: Router, private restaurantService: RestaurantService) { }

  ngOnInit(): void {
    this.divisionAddForm = this.formBuilder.group({
      // RegionName: new FormControl('', [
      //   Validators.required]),
      DivisionID: new FormControl('', [
        Validators.required,
        Validators.pattern(".{2,2}")]),
      DivisionName: new FormControl('', [
        Validators.required])
    });

    this.divisionsAddCSV = this.formBuilder.group({
      DivisionCSV: new FormControl('', [
        Validators.required]),
    });
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [0, 'asc'],
      processing: true,
      dom: 'Bfrtip',
      lengthMenu: [10, 20, 50, 100],
      buttons: [
        'print', 'csv', 'pageLength'
      ]
    };
    this.getDivisionList();
  }
  handleFileInput(file: FileList) {
    console.log("Before", file);
    this.fileToUpload = file.item(0);
    console.log("After", this.fileToUpload);
  }
  CSVSubmit() {
    debugger;
    this.restaurantService.postFile(this.fileToUpload).subscribe(
      data => {
        console.log("Fileupload Data", data);
      })
  }
  onSubmit() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);

    let body =
    {
      // RegionName: this.divisionAddForm.controls['RegionName'].value,
      DivisionID: this.divisionAddForm.controls['DivisionID'].value,
      DivisionName: this.divisionAddForm.controls['DivisionName'].value,

      //   RestaurantCategory: this.ResCategoryName,
      // //  TotalDevices: this.restaurantAddForm.controls['TotalDevices'].value,
      //   RestaurantVerified: this.restaurantAddForm.controls['RestaurantVerified'].value,
      //   CategoryID: this.ResCategoryId
      //   //CategoryID: this.CategoryList.CategoryID
    }
    debugger;
    this.restaurantService.addDivision(body).subscribe(
      data => {
        this.DivisionList = data;
        console.log("Division List: ", this.DivisionList);
        this.ngOnInit();
      },
      err => {
        debugger;
        console.log("Error  ", err);
        // this.error = err.error.data.description;
        // this.loading = false;

      });
  }
  getDivisionList() {
    this.restaurantService.getDivision().subscribe(
      data => {
        this.Divisions = data['data'];
        console.log("Get Division List: ", this.Divisions);
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();

  }
}