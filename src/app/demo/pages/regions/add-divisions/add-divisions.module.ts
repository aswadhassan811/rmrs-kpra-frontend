import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddDivisionsRoutingModule } from './add-divisions-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AddDivisionsRoutingModule
  ]
})
export class AddDivisionsModule { }
