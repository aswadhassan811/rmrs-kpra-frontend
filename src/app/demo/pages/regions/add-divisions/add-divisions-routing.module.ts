import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddDivisionsComponent } from './add-divisions.component';

const routes: Routes = [
  {
    path: '',
    component: AddDivisionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddDivisionsRoutingModule { }
