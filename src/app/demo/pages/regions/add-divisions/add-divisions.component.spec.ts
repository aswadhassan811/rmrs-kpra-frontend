import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDivisionsComponent } from './add-divisions.component';

describe('AddDivisionsComponent', () => {
  let component: AddDivisionsComponent;
  let fixture: ComponentFixture<AddDivisionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDivisionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDivisionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
