import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddCitiesRoutingModule } from './add-cities-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AddCitiesRoutingModule
  ]
})
export class AddCitiesModule { }
