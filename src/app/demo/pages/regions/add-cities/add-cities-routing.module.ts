import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCitiesComponent } from './add-cities.component';

const routes: Routes = [
  {
    path: '',
    component: AddCitiesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddCitiesRoutingModule { }
