import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-add-cities',
  templateUrl: './add-cities.component.html',
  styleUrls: ['./add-cities.component.scss']
})
export class AddCitiesComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  cityAddForm: any;
  CityList: any;
  Cities: any;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  regionsAddCSV: any;
  CityCSV: any;
  fileToUpload: File = null;
  constructor(private formBuilder: FormBuilder, private router: Router, private restaurantService: RestaurantService) { }

  ngOnInit(): void {
    this.cityAddForm = this.formBuilder.group({
      // DivisionName: new FormControl('', [
      //   Validators.required]),
      // DistrictName: new FormControl('', [
      //   Validators.required]),
      CityID: new FormControl('', [
        Validators.required]),
      CityName: new FormControl('', [
        Validators.required]),
    }
    );
    this.regionsAddCSV = this.formBuilder.group({
      CitiesCSV: new FormControl('', [
        Validators.required]),
    });
    this.getCityList();
  }

  onSubmit() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);

    let body =
    {
      // DivisionName: this.cityAddForm.controls['DivisionName'].value,
      // DistrictName: this.cityAddForm.controls['DistrictName'].value,
      CityID: this.cityAddForm.controls['CityID'].value,
      CityName: this.cityAddForm.controls['CityName'].value,

      //   RestaurantCategory: this.ResCategoryName,
      // //  TotalDevices: this.restaurantAddForm.controls['TotalDevices'].value,
      //   RestaurantVerified: this.restaurantAddForm.controls['RestaurantVerified'].value,
      //   CategoryID: this.ResCategoryId
      //   //CategoryID: this.CategoryList.CategoryID
    }
    debugger;
    this.restaurantService.addCity(body).subscribe(
      data => {
        this.CityList = data;
        console.log("City List: ", this.CityList);
        this.ngOnInit();
      },
      err => {
        debugger;
        console.log("Error  ", err);
        // this.error = err.error.data.description;
        // this.loading = false;

      });

  }
  handleFileInput(file: FileList) {
    console.log("Before", file);
    this.fileToUpload = file.item(0);
    console.log("After", this.fileToUpload);
  }
  CSVSubmit() {
    debugger;
    // this.restaurantService.addUploadCSV(body).subscribe(
    //   data => {
    //     this.CityCSV = data;
    //     console.log("Cities CSV: ", this.CityCSV);
    //     this.ngOnInit();
    //   },
    //   err => {
    //     debugger;
    //     console.log("Error  ", err);
    //   });
    this.restaurantService.postFile(this.fileToUpload).subscribe(
      data => {
        console.log("Fileupload Data", data);
      });
  }
  getCityList() {
    this.restaurantService.getCity().subscribe(
      data => {
        this.Cities = data['data'];
        console.log("Get Cities List: ", this.Cities);
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();

  }
}
