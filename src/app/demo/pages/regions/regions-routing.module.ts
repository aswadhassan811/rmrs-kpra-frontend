import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'add-regions',
      loadChildren: () => import('./add-regions/add-regions.module').then(module => module.AddRegionsModule)
    }, 
    {
      path: 'add-divisions',
      loadChildren: () => import('./add-divisions/add-divisions.module').then(module => module.AddDivisionsModule)
    }, 
    {
      path: 'add-districts',
      loadChildren: () => import('./add-districts/add-districts.module').then(module => module.AddDistrictsModule)
    }, 
    {
      path: 'add-cities',
      loadChildren: () => import('./add-cities/add-cities.module').then(module => module.AddCitiesModule)
    }, 
    {
      path: 'location-config',
      loadChildren: () => import('./location-config/location-config.module').then(module => module.LocationConfigModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegionsRoutingModule { }
