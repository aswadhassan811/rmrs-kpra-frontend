import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddRegionsRoutingModule } from './add-regions-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AddRegionsRoutingModule,
    SharedModule,
    // NgbTabsetModule
  ]
})
export class AddRegionsModule { }
