import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddRegionsComponent } from './add-regions.component';

const routes: Routes = [
  {
    path: '',
    component: AddRegionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddRegionsRoutingModule { }
