import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-add-regions',
  templateUrl: './add-regions.component.html',
  styleUrls: ['./add-regions.component.scss']
})
export class AddRegionsComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  regionAddForm: FormGroup;
  fileToUpload: File = null;
  // Location: any;
  RegionList: any;
  getRegionsList: any;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  Regions: any;
  regionsAddCSV: FormGroup;
  RegionCSV: any;
  imageUrl: string;

  constructor(private formBuilder: FormBuilder, private router: Router, private restaurantService: RestaurantService) { }

  ngOnInit(): void {
    this.regionAddForm = this.formBuilder.group({
      // RegionID: new FormControl('', [
      //   Validators.required,
      //   Validators.pattern(".{2,2}")]),
      RegionID: new FormControl('', [
        Validators.required]),
      RegionName: new FormControl('', [
        Validators.required]),
    });

    this.regionsAddCSV = this.formBuilder.group({
      RegionsCSV: new FormControl('', [
        Validators.required]),
    });

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [0, 'asc'],
      processing: true,
      dom: 'Bfrtip',
      lengthMenu: [10, 20, 50, 100],
      buttons: [
        'print', 'csv', 'pageLength'
      ]
    };
    this.getRegionList();
  }
  onSubmit() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);

    let body =
    {
      RegionID: this.regionAddForm.controls['RegionID'].value,
      RegionName: this.regionAddForm.controls['RegionName'].value,

      //   RestaurantCategory: this.ResCategoryName,
      // //  TotalDevices: this.restaurantAddForm.controls['TotalDevices'].value,
      //   RestaurantVerified: this.restaurantAddForm.controls['RestaurantVerified'].value,
      //   CategoryID: this.ResCategoryId
      //   //CategoryID: this.CategoryList.CategoryID
    }
    debugger;
    this.restaurantService.addRegions(body).subscribe(
      data => {
        this.RegionList = data;
        console.log("Region List: ", this.RegionList);
        this.ngOnInit();
      },
      err => {
        debugger;
        console.log("Error  ", err);
        // this.error = err.error.data.description;
        // this.loading = false;

      });
  }
  handleFileInput(file: FileList) {
    console.log("Before", file);
    this.fileToUpload = file.item(0);
    console.log("After", this.fileToUpload);
  }
  // Uploadimage() {
  //   this.restaurantService.postFile(this.fileToUpload).subscribe(
  //     data => {
  //       //  Caption.value = null;
  //       // Image.value = null;
  //       this.imageUrl = data['Imageurl'];
  //     }
  //   );
  // }
  CSVSubmit() {
    console.log("Regions CSV", this.regionsAddCSV.controls['RegionsCSV'])
    debugger;
    this.restaurantService.postFile(this.fileToUpload).subscribe(
          data => {
            console.log("Fileupload Data", data);
          })
  }
  getRegionList() {
    this.restaurantService.getRegions().subscribe(
      data => {
        this.Regions = data['data'];
        console.log("Get Region List: ", this.Regions);
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      },
      err => {
      }
    );
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();

  }
}
