import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LocationConfigComponent } from './location-config.component';

const routes: Routes = [
  {
    path: '',
    component: LocationConfigComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationConfigRoutingModule { }
