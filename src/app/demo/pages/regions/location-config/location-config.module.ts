import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocationConfigRoutingModule } from './location-config-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LocationConfigRoutingModule
  ]
})
export class LocationConfigModule { }
