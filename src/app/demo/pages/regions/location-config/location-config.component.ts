import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { DataTableDirective } from 'angular-datatables';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-location-config',
  templateUrl: './location-config.component.html',
  styleUrls: ['./location-config.component.scss']
})
export class LocationConfigComponent implements OnInit {
  dropdownSettings: IDropdownSettings;

  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  locationConfigForm: any;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  LocationList: any;
  Regions: any;
  Locations: any;
  Divisions: any;
  Districts: any;
  CityList: any;
  RegionID: any;
  //CityName: any;
  //dropdownSettings: any;
  constructor(private formBuilder: FormBuilder, private router: Router, private restaurantService: RestaurantService) { }
  dropdownList = [];
  selectedItems = [];
  //  dropdownSettings = {};
  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [0, 'asc'],
      processing: true,
      dom: 'Bfrtip',
      lengthMenu: [10, 20, 50, 100],
      buttons: [
        'print', 'csv', 'pageLength'
      ]
    };
    this.locationConfigForm = this.formBuilder.group({
      RegionName: new FormControl('', [
        Validators.required]),
      DivisionName: new FormControl('', [
        Validators.required]),
      DistrictName: new FormControl('', [
        Validators.required]),
      CityName: new FormControl('', [
        Validators.required]),
    });

    this.getLocationList();
    this.getRegionList();
    this.getDivisionList();
    this.getDistrictList();
    this.getCityList();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    }
  }
  onSubmit() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);
    debugger;
    const val = this.locationConfigForm.controls['RegionName'];
    let body =
    {
      RegionName: this.locationConfigForm.controls['RegionName'].value,
      DivisionName: this.locationConfigForm.controls['DivisionName'].value,
      DistrictName: this.locationConfigForm.controls['DistrictName'].value,
      Cities: this.locationConfigForm.controls['CityName'].value,
    }
    debugger;
    console.log(this.RegionID)
    this.restaurantService.addLocation(body).subscribe(
      data => {
        debugger
        this.LocationList = data['data'];
        console.log("Location List: ", this.LocationList);
        debugger
        this.ngOnInit();
      },
      err => {
        debugger;
        console.log("Error  ", err);

      });
  }

  getLocationList() {
    this.restaurantService.getLocation().subscribe(
      (data: any) => {
        debugger
        console.log(data)
        let tmpData = []

        for (let i = 0; i < data.data.length; i++) {

          for (let j = 0; j < data.data[i].Cities.length; j++) {
            let safi = {
              RegionName: data.data[i].RegionName,
              DivisionName: data.data[i].DivisionName,
              DistrictName: data.data[i].DistrictName,
              Cities: data.data[i].Cities[j].item_text,
            }
            tmpData.push(safi)
          }
        }

        console.log("laloprasad sharma", data.data)
        this.Locations = tmpData;
        console.log("Get Location List: ", this.Locations);
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getRegionList() {
    this.restaurantService.getRegions().subscribe(
      data => {
        this.Regions = data['data'];
        console.log("Get Region List: ", this.Regions);
        // Calling the DT trigger to manually render the table
        //  this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getDivisionList() {
    this.restaurantService.getDivision().subscribe(
      data => {
        this.Divisions = data['data'];
        console.log("Get Division List: ", this.Divisions);
        // Calling the DT trigger to manually render the table
        //  this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getDistrictList() {
    this.restaurantService.getDistrict().subscribe(
      data => {
        debugger
        this.Districts = data['data'];
        console.log("Get District List: ", this.Districts);
        // Calling the DT trigger to manually render the table
        // this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getCityList() {
    this.restaurantService.getCity().subscribe(
      data => {
        //console.log(data['data'])
        let cities_data = []
        for (let i = 0; i < data['data'].length; i++) {
          // console.log(data['data'])
          cities_data.push({ item_id: data['data'][i].CityID, item_text: data['data'][i].CityName })
        }
        this.CityList = cities_data;
        console.log("Get City List: ", this.CityList);
        // Calling the DT trigger to manually render the table
        // this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();

  }
  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
}
