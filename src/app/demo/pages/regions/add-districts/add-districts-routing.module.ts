import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddDistrictsComponent } from './add-districts.component';

const routes: Routes = [
  {
    path: '',
    component: AddDistrictsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddDistrictsRoutingModule { }
