import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddDistrictsRoutingModule } from './add-districts-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AddDistrictsRoutingModule
  ]
})
export class AddDistrictsModule { }
