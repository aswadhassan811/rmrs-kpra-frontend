import { Component, OnInit, ViewChild } from '@angular/core';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-add-districts',
  templateUrl: './add-districts.component.html',
  styleUrls: ['./add-districts.component.scss']
})
export class AddDistrictsComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  districtAddForm: FormGroup;
  fileToUpload: File = null;
  Location: any;
  Districts: any;
  getDistrictsList: any;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  regionsAddCSV: FormGroup;
  RegionCSV: any;
  constructor(private formBuilder: FormBuilder, private router: Router, private restaurantService: RestaurantService) { }

  ngOnInit(): void {
    this.districtAddForm = this.formBuilder.group({
      // DivisionName: new FormControl('', [
      //   Validators.required]),
      DistrictID: new FormControl('', [
        Validators.required]),
      DistrictName: new FormControl('', [
        Validators.required])
    });
    this.regionsAddCSV = this.formBuilder.group({
      DistrictsCSV: new FormControl('', [
        Validators.required]),
    });
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [0, 'asc'],
      processing: true,
      dom: 'Bfrtip',
      lengthMenu: [10, 20, 50, 100],
      buttons: [
        'print', 'csv', 'pageLength'
      ]
    };
    this.getDistrictList();
  }
  handleFileInput(file: FileList) {
    console.log("Before", file);
    this.fileToUpload = file.item(0);
    console.log("After", this.fileToUpload);
  }
  CSVSubmit() {
    debugger;
    this.restaurantService.postFile(this.fileToUpload).subscribe(
      data => {
        console.log("Fileupload Data", data);
      })
  }
  onSubmit() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);

    let body =
    {
      // DivisionName: this.districtAddForm.controls['DivisionName'].value,
      DistrictID: this.districtAddForm.controls['DistrictID'].value,
      DistrictName: this.districtAddForm.controls['DistrictName'].value,

      //   RestaurantCategory: this.ResCategoryName,
      // //  TotalDevices: this.restaurantAddForm.controls['TotalDevices'].value,
      //   RestaurantVerified: this.restaurantAddForm.controls['RestaurantVerified'].value,
      //   CategoryID: this.ResCategoryId
      //   //CategoryID: this.CategoryList.CategoryID
    }
    debugger;
    this.restaurantService.addDistrict(body).subscribe(
      data => {
        this.Districts = data;
        console.log("Districts List: ", this.Districts);
        this.ngOnInit();
      },
      err => {
        debugger;
        console.log("Error  ", err);
        // this.error = err.error.data.description;
        // this.loading = false;

      });
  }
  getDistrictList() {
    this.restaurantService.getDistrict().subscribe(
      data => {
        debugger
        this.Districts = data['data'];
        console.log("Get District List: ", this.Districts);
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();

  }
}
