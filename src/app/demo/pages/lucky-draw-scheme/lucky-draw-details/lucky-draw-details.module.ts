import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LuckyDrawDetailsRoutingModule } from './lucky-draw-details-routing.module';
import { LuckyDrawDetailsComponent } from './lucky-draw-details.component';
import { DataTablesModule } from 'angular-datatables';
import { SharedModule } from 'src/app/theme/shared/shared.module';


@NgModule({
  declarations: [
    LuckyDrawDetailsComponent
  ],
  imports: [
    CommonModule,
    LuckyDrawDetailsRoutingModule,
    SharedModule,
    DataTablesModule
  ]
})
export class LuckyDrawDetailsModule { }
