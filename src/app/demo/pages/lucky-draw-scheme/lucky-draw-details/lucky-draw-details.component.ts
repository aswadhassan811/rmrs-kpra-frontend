import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
@Component({
  selector: 'app-lucky-draw-details',
  templateUrl: './lucky-draw-details.component.html',
  styleUrls: ['./lucky-draw-details.component.scss']
})
export class LuckyDrawDetailsComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  LuckyDrawDetailList: any= {LuckyDrawID: '', LDName: '', PointsMinRange: '', PointsMaxRange: '', StartDate: '', EndDate: ''};
  luckyDrawID: any;
  // LuckyDrawgetParticipantsList: any;
  LuckyDrawParticipantsList: any;
  LuckyDrawWinner: any= {Name: ''};
  clicked = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private restaurantService: RestaurantService) { }

  ngOnInit(): void {
    this.luckyDrawID = localStorage.getItem("luckyDrawID");
    console.log(localStorage.getItem("luckyDrawID"));
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [0, 'asc'],
      processing: true,
      dom: 'Bfrtip',
      lengthMenu: [10, 20, 50, 100],
      buttons: [
        'print', 'csv', 'pageLength'
      ]
    };
    this.getLuckyDrawDetails();
    this.getLuckyDrawParticipant();
    this.runLottery();
  }
  getLuckyDrawParticipant() {
    this.restaurantService.getParticipants(this.luckyDrawID).subscribe(
      data => {
        this.LuckyDrawParticipantsList = data;
        console.log("Get lucky draw participant list: ", this.LuckyDrawParticipantsList);
        // console.log("Get lucky draw details data: ", data);
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getLuckyDrawDetails() {
    this.restaurantService.getLuckyDrawDetail(this.luckyDrawID).subscribe(
      data => {
        this.LuckyDrawDetailList = data['data'];

        console.log("Get lucky draw details list: ", this.LuckyDrawDetailList);
        // console.log("Get lucky draw details data: ", data);
        // Calling the DT trigger to manually render the table
       // this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  runLottery() {
    this.restaurantService.getRunLottery(this.luckyDrawID).subscribe(
      data => {
        this.LuckyDrawWinner = data['data'];
        console.log("Lucky Draw winner list: ", this.LuckyDrawWinner);
        // console.log("Get lucky draw details data: ", data);
        // Calling the DT trigger to manually render the table
       // this.dtTrigger.next();
      //  this.clicked = true;
      },
      
      err => {
      }
    );
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();

  }
}


