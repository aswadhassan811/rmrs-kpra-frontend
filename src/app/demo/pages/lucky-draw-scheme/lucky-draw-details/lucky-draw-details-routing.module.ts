import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LuckyDrawDetailsComponent } from './lucky-draw-details.component';

const routes: Routes = [
  {
    path: '',
    component: LuckyDrawDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LuckyDrawDetailsRoutingModule { }
