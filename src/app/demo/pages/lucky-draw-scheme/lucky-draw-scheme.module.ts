import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LuckyDrawSchemeRoutingModule } from './lucky-draw-scheme-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LuckyDrawSchemeRoutingModule,
    SharedModule,
    DataTablesModule,
  ]
})
export class LuckyDrawSchemeModule { }
