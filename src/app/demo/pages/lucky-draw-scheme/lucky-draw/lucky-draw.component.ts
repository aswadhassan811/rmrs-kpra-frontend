import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
@Component({
  selector: 'app-lucky-draw',
  templateUrl: './lucky-draw.component.html',
  styleUrls: ['./lucky-draw.component.scss']
})
export class LuckyDrawComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  luckyDrawAddForm: any;
  LuckyDrawList: any;
  LuckyDraw: any;
  LuckyDrawData: any;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();

  constructor(private formBuilder: FormBuilder, private router: Router, private restaurantService: RestaurantService) { }

  ngOnInit(): void {
    this.luckyDrawAddForm = this.formBuilder.group({
      luckyDrawName: new FormControl('', [
        Validators.required]),
      startDate: new FormControl('', [
        Validators.required]),
      endDate: new FormControl('', [
        Validators.required]),
      pointMinRange: new FormControl('', [
        Validators.required]),
      pointMaxRange: new FormControl('', [
        Validators.required]),
    });
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [0, 'asc'],
      processing: true,
      dom: 'Bfrtip',
      lengthMenu: [10, 20, 50, 100],
      buttons: [
        'print', 'csv', 'pageLength'
      ]
    };
    this.getLuckyDraw();
  }

  onSubmit() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);
    debugger;
    let body =
    {

      // RegionID: this.RegionID,

      LDName: this.luckyDrawAddForm.controls['luckyDrawName'].value,
      StartDate: this.luckyDrawAddForm.controls['startDate'].value,
      EndDate: this.luckyDrawAddForm.controls['endDate'].value,
      PointsMinRange: this.luckyDrawAddForm.controls['pointMinRange'].value,
      PointsMaxRange: this.luckyDrawAddForm.controls['pointMaxRange'].value,

      //   RestaurantCategory: this.ResCategoryName,
      // //  TotalDevices: this.restaurantAddForm.controls['TotalDevices'].value,
      //   RestaurantVerified: this.restaurantAddForm.controls['RestaurantVerified'].value,
      //   CategoryID: this.ResCategoryId
      //   //CategoryID: this.CategoryList.CategoryID
    }
    console.log("Lucky draw body:", body)
    debugger;
    //console.log(this.RegionID)
    this.restaurantService.addLuckyDraw(body).subscribe(
      data => {
        debugger
        this.LuckyDrawData = data['data'];
        console.log("Lucky Draw List: ", this.LuckyDrawData);
        debugger
        this.ngOnInit();
      },
      err => {
        debugger;
        console.log("Error  ", err);
        // this.error = err.error.data.description;
        // this.loading = false;

      });
  }
  getLuckyDraw() {
    this.restaurantService.getLuckyDraw().subscribe(
      data => {
        this.LuckyDrawList = data['data'];
        console.log("Get lucky draw list: ", this.LuckyDrawList);
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();
  }

  viewLuckyDraw(LuckyDrawID: any) {
    // this.restriction = true;
    localStorage.setItem("luckyDrawID", LuckyDrawID);
    // this.restaurantService.sendFlag(this.restriction);
    // this.restaurantService.updateLuckyDrawID(LuckyDrawID);
    // this.foo = true;
    // localStorage.setItem("foo", this.foo);
    this.router.navigate(['/lucky-draw-scheme/lucky-draw-details']);
  }
}
