import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LuckyDrawComponent } from './lucky-draw.component';

const routes: Routes = [
  {
    path: '',
    component: LuckyDrawComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LuckyDrawRoutingModule { }
