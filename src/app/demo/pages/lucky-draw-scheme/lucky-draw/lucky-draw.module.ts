import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LuckyDrawRoutingModule } from './lucky-draw-routing.module';
import { LuckyDrawComponent } from './lucky-draw.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  declarations: [
    LuckyDrawComponent
  ],
  imports: [
    CommonModule,
    LuckyDrawRoutingModule,
    SharedModule,
    DataTablesModule
  ]
})
export class LuckyDrawModule { }
