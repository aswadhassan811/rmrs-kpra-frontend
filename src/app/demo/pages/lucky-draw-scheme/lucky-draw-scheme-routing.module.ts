import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'lucky-draw',
        loadChildren: () => import('./lucky-draw/lucky-draw.module').then(module => module.LuckyDrawModule)
      },
      {
        path: 'lucky-draw-details',
        loadChildren: () => import('./lucky-draw-details/lucky-draw-details.module').then(module => module.LuckyDrawDetailsModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LuckyDrawSchemeRoutingModule { }
