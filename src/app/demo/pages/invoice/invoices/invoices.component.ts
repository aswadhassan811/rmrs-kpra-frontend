
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {
  // Must be declared as "any", not as "DataTables.Settings"
  //dtOptions: DataTables.Settings = {};
  dtOptions: any = {};
  invoices: any;
  dtTrigger: Subject<any> = new Subject<any>();

  constructor(private restaurantService: RestaurantService) { }


  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [1, 'desc'],
      processing: true,
      dom: 'Bfrtip',
      lengthMenu: [10, 20, 50, 100],
      buttons: [
        'print', 'csv', 'pageLength'
      ]
    };
    this.getSalesReports();

  }


  getSalesReports() {
    // debugger;
    // this.http.get('https://kpra.teletaleem.com/invoice')


    this.restaurantService.AllRestaurantsInvocies().subscribe(
      res => {
        this.invoices = res["data"];
        console.log("Data:", res["data"])
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      });

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();

  }


}



