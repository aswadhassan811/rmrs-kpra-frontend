import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuspectedDiscrepancyReportsComponent } from './suspected-discrepancy-reports.component';

describe('SuspectedDiscrepancyReportsComponent', () => {
  let component: SuspectedDiscrepancyReportsComponent;
  let fixture: ComponentFixture<SuspectedDiscrepancyReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuspectedDiscrepancyReportsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuspectedDiscrepancyReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
