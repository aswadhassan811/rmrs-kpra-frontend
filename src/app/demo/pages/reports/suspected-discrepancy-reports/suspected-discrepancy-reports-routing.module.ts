import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuspectedDiscrepancyReportsComponent } from './suspected-discrepancy-reports.component';

const routes: Routes = [
  {
    path: '',
    component: SuspectedDiscrepancyReportsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuspectedDiscrepancyReportsRoutingModule { }
