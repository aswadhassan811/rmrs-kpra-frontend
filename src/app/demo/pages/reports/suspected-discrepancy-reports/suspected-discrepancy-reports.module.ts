import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuspectedDiscrepancyReportsRoutingModule } from './suspected-discrepancy-reports-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SuspectedDiscrepancyReportsRoutingModule,
    SharedModule
  ]
})
export class SuspectedDiscrepancyReportsModule { }
