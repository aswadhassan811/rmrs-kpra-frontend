import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComparisonSalesReportRoutingModule } from './comparison-sales-report-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ComparisonSalesReportRoutingModule,
    SharedModule,
    NgbTabsetModule
  ]
})
export class ComparisonSalesReportModule { }
