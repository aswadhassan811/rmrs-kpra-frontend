import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ApexChartService } from 'src/app/theme/shared/components/chart/apex-chart/apex-chart.service';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { ChartComponent, ApexAxisChartSeries, ApexChart, ApexXAxis, ApexTitleSubtitle, ApexDataLabels, ApexStroke, ApexTooltip } from "ng-apexcharts";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  title: ApexTitleSubtitle;
  dataLabels: ApexDataLabels;
  stroke: ApexStroke;
  tooltip: ApexTooltip
};

@Component({
  selector: 'app-comparison-sales-report',
  templateUrl: './comparison-sales-report.component.html',
  styleUrls: ['./comparison-sales-report.component.scss']
})
export class ComparisonSalesReportComponent implements OnInit {
  @ViewChild("chart", { static: false }) chart: ChartComponent;
  public chartDB: Partial<ChartOptions>;

  public lastDate: number;
  public line2CAC: any;
  public data: any;

  public intervalSub: any;
  public intervalMain: any;

  public dailyVisitorStatus: string;
  public dailyVisitorAxis: any;
  RegionGraphicalData: any;
  Regions: any;
  Divisions: any;
  Districts: any;
  CityList: any;
  RestaurantList: any;
  regionReportsForm: any;
  RegionReportsAdd: any;
  RegionName: string;
  y1_axis: any;
  y2_axis: any;
  RegionReportsAddDaily: any;
  RegionGraphicalDataDaily: any;
  divisionReportsForm: any;
  DivisionReportsAdd: any;
  DivisionGraphicalData: any;
  DivisionReportsAddDaily: any;
  DivisionGraphicalDataDaily: any;
  districtReportsForm: any;
  DistrictReportsAdd: any;
  DistrictGraphicalData: any;
  DistrictReportsAddDaily: any;
  DistrictGraphicalDataDaily: any;
  RegionReportsAddMonthly: any;
  RegionGraphicalDataMonthly: any;
  DivisionReportsAddMonthly: any;
  DivisionGraphicalDataMonthly: any;
  DistrictReportsAddMonthly: any;
  DistrictGraphicalDataMonthly: any;
  cityReportsForm: any;
  CityReportsAdd: any;
  CityGraphicalData: any;
  CityReportsAddMonthly: any;
  CityGraphicalDataMonthly: any;
  CityReportsAddDaily: any;
  CityGraphicalDataDaily: any;
  restaurantReportsForm: any;
  RestaurantReportsAdd: any;
  RestaurantGraphicalData: any;
  RestaurantReportsAddMonthly: any;
  RestaurantGraphicalDataMonthly: any;
  RestaurantReportsAddDaily: any;
  RestaurantGraphicalDataDaily: any;
  isCheck: boolean = false;
  activeClassD: string;
  activeClassM: string;
  activeClass: string;
  constructor(public apexEvent: ApexChartService, private formBuilder: FormBuilder, private restaurantService: RestaurantService) {

    this.chartDB = {
      chart: {
        height: 350,
        type: 'area',
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      // series: [{
      //   name: 'Sales',
      //   data: [21, 1232, 43, 23, 32]
      // }, {
      //   name: 'Sales Tax',
      //   data: [21, 12, 43, 23, 32]
      // }],

      // xaxis: {
      //   categories: [2017, 2016, 2018, 2019, 2020, 2021],
      // },
      tooltip: {
        x: {
          format: 'dd/MM/yy HH:mm'
        },
      }
    };
  }

  ngOnInit(): void {

    this.getRegionList();
    this.getDivisionList();
    this.getDistrictList();
    this.getCityList();
    this.getRestaurantList();

    this.regionReportsForm = this.formBuilder.group({
      RegionName: new FormControl(''),
    });
    this.divisionReportsForm = this.formBuilder.group({
      DivisionName: new FormControl(''),
    });
    this.districtReportsForm = this.formBuilder.group({
      DistrictName: new FormControl(''),
    });
    this.cityReportsForm = this.formBuilder.group({
      CityName: new FormControl(''),
    });
    this.restaurantReportsForm = this.formBuilder.group({
      RestaurantName: new FormControl(''),
    });
  }

  onSubmit() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);
    debugger;
    const val = this.regionReportsForm.controls['RegionName'];
    let body =
    {
      RegionName: this.regionReportsForm.controls['RegionName'].value,
    }
    debugger;
    this.restaurantService.searchRegionReports(body).subscribe(
      data => {
        debugger
        let x_axis: any = []
        let y1_axis: any = []
        let y2_axis: any = []
        this.RegionReportsAdd = data['data']['Yearly'];
        for (let i = 0; i < this.RegionReportsAdd.length; i++) {
          x_axis.push(this.RegionReportsAdd[i].Year)
          y1_axis.push(this.RegionReportsAdd[i].TotalAmount)
          y2_axis.push(this.RegionReportsAdd[i].SalesTax)
        }
        this.RegionGraphicalData = [{
          name: 'Sales Amount',
          data: y1_axis
        }, {
          name: 'Sales Tax',
          data: y2_axis
        }, {
          categories: x_axis,
        }]

        debugger
        let Mx_axis: any = []
        let My1_axis: any = []
        let My2_axis: any = []
        this.RegionReportsAddMonthly = data['data']['Monthly'];
        for (let i = 0; i < this.RegionReportsAddMonthly.length; i++) {
          Mx_axis.push(this.RegionReportsAddMonthly[i].Month)
          My1_axis.push(this.RegionReportsAddMonthly[i].TotalAmount)
          My2_axis.push(this.RegionReportsAddMonthly[i].SalesTax)
        }
        this.RegionGraphicalDataMonthly = [{
          name: 'Sales Amount',
          data: My1_axis
        }, {
          name: 'Sales Tax',
          data: My2_axis
        }, {
          categories: Mx_axis,
        }]

        let Dx_axis: any = []
        let Dy1_axis: any = []
        let Dy2_axis: any = []
        this.RegionReportsAddDaily = data['data']['Daily'];
        for (let i = 0; i < this.RegionReportsAddDaily.length; i++) {
          Dx_axis.push(this.RegionReportsAddDaily[i].Day)
          Dy1_axis.push(this.RegionReportsAddDaily[i].TotalAmount)
          Dy2_axis.push(this.RegionReportsAddDaily[i].SalesTax)
        }
        console.log(Dx_axis)
        console.log(Dy1_axis)
        console.log(Dy2_axis)
        this.RegionGraphicalDataDaily = [{
          name: 'Sales Amount',
          data: Dy1_axis
        }, {
          name: 'Sales Tax',
          data: Dy2_axis
        }, {
          categories: Dx_axis,
        }]

        // this.chartDB.series = [{
        //   name: 'Sales Amount', 
        //   data:  y1_axis
        //   }, {
        //     name: 'Sales Tax',
        //     data:y2_axis
        //   }]       
        // this.chartDB.xaxis = {
        //   categories:x_axis,
        // } 
        console.log("Region report Data: ", this.RegionReportsAdd);
        debugger
        this.RegionGraphDaily()
      },
      err => {
        debugger;
        console.log("Error  ", err);

      });
    this.isCheck = true;
  }
  RegionGraphDaily() {
    this.chartDB.series = [this.RegionGraphicalDataDaily[0], this.RegionGraphicalDataDaily[1]]
    this.chartDB.xaxis = this.RegionGraphicalDataDaily[2]
    this.isCheck = true;
    this.activeClassD ="active";
    this.activeClassM ="";
    this.activeClass ="";
    
  }

  RegionGraphMonthly() {
    this.chartDB.series = [this.RegionGraphicalDataMonthly[0], this.RegionGraphicalDataMonthly[1]]
    this.chartDB.xaxis = this.RegionGraphicalDataMonthly[2]
    this.isCheck = true;
    this.activeClassD ="";
    this.activeClassM ="active";
    this.activeClass ="";
    
  }

  RegionGraph() {
    this.chartDB.series = [this.RegionGraphicalData[0], this.RegionGraphicalData[1]]
    this.chartDB.xaxis = this.RegionGraphicalData[2]
    this.isCheck = true;
    this.activeClassD ="";
    this.activeClassM ="";
    this.activeClass ="active";
  }

  onSubmitD() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);
    debugger;
    const val = this.divisionReportsForm.controls['DivisionName'];
    let body =
    {
      DivisionName: this.divisionReportsForm.controls['DivisionName'].value,
    }
    debugger;
    this.restaurantService.searchDivisionReports(body).subscribe(
      data => {
        debugger
        let x_axis: any = []
        let y1_axis: any = []
        let y2_axis: any = []
        this.DivisionReportsAdd = data['data']['Yearly'];
        for (let i = 0; i < this.DivisionReportsAdd.length; i++) {
          x_axis.push(this.DivisionReportsAdd[i].Year)
          y1_axis.push(this.DivisionReportsAdd[i].TotalAmount)
          y2_axis.push(this.DivisionReportsAdd[i].SalesTax)
        }
        this.DivisionGraphicalData = [{
          name: 'Sales Amount',
          data: y1_axis
        }, {
          name: 'Sales Tax',
          data: y2_axis
        }, {
          categories: x_axis,
        }]

        debugger
        let Mx_axis: any = []
        let My1_axis: any = []
        let My2_axis: any = []
        this.DivisionReportsAddMonthly = data['data']['Monthly'];
        for (let i = 0; i < this.DivisionReportsAddMonthly.length; i++) {
          Mx_axis.push(this.DivisionReportsAddMonthly[i].Month)
          My1_axis.push(this.DivisionReportsAddMonthly[i].TotalAmount)
          My2_axis.push(this.DivisionReportsAddMonthly[i].SalesTax)
        }
        this.DivisionGraphicalDataMonthly = [{
          name: 'Sales Amount',
          data: My1_axis
        }, {
          name: 'Sales Tax',
          data: My2_axis
        }, {
          categories: Mx_axis,
        }]

        let Dx_axis: any = []
        let Dy1_axis: any = []
        let Dy2_axis: any = []
        this.DivisionReportsAddDaily = data['data']['Daily'];
        for (let i = 0; i < this.DivisionReportsAddDaily.length; i++) {
          Dx_axis.push(this.DivisionReportsAddDaily[i].Day)
          Dy1_axis.push(this.DivisionReportsAddDaily[i].TotalAmount)
          Dy2_axis.push(this.DivisionReportsAddDaily[i].SalesTax)
        }
        console.log(Dx_axis)
        console.log(Dy1_axis)
        console.log(Dy2_axis)
        this.DivisionGraphicalDataDaily = [{
          name: 'Sales Amount',
          data: Dy1_axis
        }, {
          name: 'Sales Tax',
          data: Dy2_axis
        }, {
          categories: Dx_axis,
        }]
        console.log("Region report Data: ", this.DivisionReportsAdd);
        debugger
        this.DivisionGraphDaily();
      },
      err => {
        debugger;
        console.log("Error  ", err);

      });
    this.isCheck = true;
  }
  DivisionGraphDaily() {
    this.chartDB.series = [this.DivisionGraphicalDataDaily[0], this.DivisionGraphicalDataDaily[1]]
    this.chartDB.xaxis = this.DivisionGraphicalDataDaily[2]
    this.isCheck = true;
    this.activeClassD ="active";
    this.activeClassM ="";
    this.activeClass ="";
  }
  DivisionGraphMonthly() {
    this.chartDB.series = [this.DivisionGraphicalDataMonthly[0], this.DivisionGraphicalDataMonthly[1]]
    this.chartDB.xaxis = this.DivisionGraphicalDataMonthly[2]
    this.isCheck = true;
    this.activeClassD ="";
    this.activeClassM ="active";
    this.activeClass ="";
  }
  DivisionGraph() {
    this.chartDB.series = [this.DivisionGraphicalData[0], this.DivisionGraphicalData[1]]
    this.chartDB.xaxis = this.DivisionGraphicalData[2]
    this.isCheck = true;
    this.activeClassD ="";
    this.activeClassM ="";
    this.activeClass ="active";
  }

  onSubmitDistrict() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);
    debugger;
    const val = this.districtReportsForm.controls['DistrictName'];
    let body =
    {
      DistrictName: this.districtReportsForm.controls['DistrictName'].value,
    }
    debugger;
    this.restaurantService.searchDistrictReports(body).subscribe(
      data => {
        debugger
        let x_axis: any = []
        let y1_axis: any = []
        let y2_axis: any = []
        this.DistrictReportsAdd = data['data']['Yearly'];
        for (let i = 0; i < this.DistrictReportsAdd.length; i++) {
          x_axis.push(this.DistrictReportsAdd[i].Year)
          y1_axis.push(this.DistrictReportsAdd[i].TotalAmount)
          y2_axis.push(this.DistrictReportsAdd[i].SalesTax)
        }
        this.DistrictGraphicalData = [{
          name: 'Sales Amount',
          data: y1_axis
        }, {
          name: 'Sales Tax',
          data: y2_axis
        }, {
          categories: x_axis,
        }]

        debugger
        let Mx_axis: any = []
        let My1_axis: any = []
        let My2_axis: any = []
        this.DistrictReportsAddMonthly = data['data']['Monthly'];
        for (let i = 0; i < this.DistrictReportsAddMonthly.length; i++) {
          Mx_axis.push(this.DistrictReportsAddMonthly[i].Month)
          My1_axis.push(this.DistrictReportsAddMonthly[i].TotalAmount)
          My2_axis.push(this.DistrictReportsAddMonthly[i].SalesTax)
        }
        this.DistrictGraphicalDataMonthly = [{
          name: 'Sales Amount',
          data: My1_axis
        }, {
          name: 'Sales Tax',
          data: My2_axis
        }, {
          categories: Mx_axis,
        }]

        let Dx_axis: any = []
        let Dy1_axis: any = []
        let Dy2_axis: any = []
        this.DistrictReportsAddDaily = data['data']['Daily'];
        for (let i = 0; i < this.DistrictReportsAddDaily.length; i++) {
          Dx_axis.push(this.DistrictReportsAddDaily[i].Day)
          Dy1_axis.push(this.DistrictReportsAddDaily[i].TotalAmount)
          Dy2_axis.push(this.DistrictReportsAddDaily[i].SalesTax)
        }
        console.log(Dx_axis)
        console.log(Dy1_axis)
        console.log(Dy2_axis)
        this.DistrictGraphicalDataDaily = [{
          name: 'Sales Amount',
          data: Dy1_axis
        }, {
          name: 'Sales Tax',
          data: Dy2_axis
        }, {
          categories: Dx_axis,
        }]

        // this.chartDB.series = [{
        //   name: 'Sales Amount', 
        //   data:  y1_axis
        //   }, {
        //     name: 'Sales Tax',
        //     data:y2_axis
        //   }]       
        // this.chartDB.xaxis = {
        //   categories:x_axis,
        // } 
        console.log("District report Data: ", this.DistrictReportsAdd);
        debugger
        this.DistrictGraphDaily();
      },
      err => {
        debugger;
        console.log("Error  ", err);

      });
    this.isCheck = true;
  }
  DistrictGraphDaily() {
    this.chartDB.series = [this.DistrictGraphicalDataDaily[0], this.DistrictGraphicalDataDaily[1]]
    this.chartDB.xaxis = this.DistrictGraphicalDataDaily[2]
    this.isCheck = true;
    this.activeClassD ="active";
    this.activeClassM ="";
    this.activeClass ="";
  }
  DistrictGraphMonthly() {
    this.chartDB.series = [this.DistrictGraphicalDataMonthly[0], this.DistrictGraphicalDataMonthly[1]]
    this.chartDB.xaxis = this.DistrictGraphicalDataMonthly[2]
    this.isCheck = true;
    this.activeClassD ="";
    this.activeClassM ="active";
    this.activeClass ="";
  }

  DistrictGraph() {
    this.chartDB.series = [this.DistrictGraphicalData[0], this.DistrictGraphicalData[1]]
    this.chartDB.xaxis = this.DistrictGraphicalData[2]
    this.isCheck = true;
    this.activeClassD ="";
    this.activeClassM ="";
    this.activeClass ="active";
  }

  onSubmitCity() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);
    debugger;
    const val = this.cityReportsForm.controls['CityName'];
    let body =
    {
      CityName: this.cityReportsForm.controls['CityName'].value,
    }
    debugger;
    this.restaurantService.searchCityReports(body).subscribe(
      data => {
        debugger
        let x_axis: any = []
        let y1_axis: any = []
        let y2_axis: any = []
        this.CityReportsAdd = data['data']['Yearly'];
        for (let i = 0; i < this.CityReportsAdd.length; i++) {
          x_axis.push(this.CityReportsAdd[i].Year)
          y1_axis.push(this.CityReportsAdd[i].TotalAmount)
          y2_axis.push(this.CityReportsAdd[i].SalesTax)
        }
        this.CityGraphicalData = [{
          name: 'Sales Amount',
          data: y1_axis
        }, {
          name: 'Sales Tax',
          data: y2_axis
        }, {
          categories: x_axis,
        }]

        debugger
        let Mx_axis: any = []
        let My1_axis: any = []
        let My2_axis: any = []
        this.CityReportsAddMonthly = data['data']['Monthly'];
        for (let i = 0; i < this.CityReportsAddMonthly.length; i++) {
          Mx_axis.push(this.CityReportsAddMonthly[i].Month)
          My1_axis.push(this.CityReportsAddMonthly[i].TotalAmount)
          My2_axis.push(this.CityReportsAddMonthly[i].SalesTax)
        }
        this.CityGraphicalDataMonthly = [{
          name: 'Sales Amount',
          data: My1_axis
        }, {
          name: 'Sales Tax',
          data: My2_axis
        }, {
          categories: Mx_axis,
        }]

        let Dx_axis: any = []
        let Dy1_axis: any = []
        let Dy2_axis: any = []
        this.CityReportsAddDaily = data['data']['Daily'];
        for (let i = 0; i < this.CityReportsAddDaily.length; i++) {
          Dx_axis.push(this.CityReportsAddDaily[i].Day)
          Dy1_axis.push(this.CityReportsAddDaily[i].TotalAmount)
          Dy2_axis.push(this.CityReportsAddDaily[i].SalesTax)
        }
        console.log(Dx_axis)
        console.log(Dy1_axis)
        console.log(Dy2_axis)
        this.CityGraphicalDataDaily = [{
          name: 'Sales Amount',
          data: Dy1_axis
        }, {
          name: 'Sales Tax',
          data: Dy2_axis
        }, {
          categories: Dx_axis,
        }]

        // this.chartDB.series = [{
        //   name: 'Sales Amount', 
        //   data:  y1_axis
        //   }, {
        //     name: 'Sales Tax',
        //     data:y2_axis
        //   }]       
        // this.chartDB.xaxis = {
        //   categories:x_axis,
        // } 
        console.log("City report Data: ", this.CityReportsAdd);
        debugger
        this.CityGraphDaily();
      },
      err => {
        debugger;
        console.log("Error  ", err);

      });
    this.isCheck = true;
  }
  CityGraphDaily() {
    this.chartDB.series = [this.CityGraphicalDataDaily[0], this.CityGraphicalDataDaily[1]]
    this.chartDB.xaxis = this.CityGraphicalDataDaily[2]
    this.isCheck = true;
    this.activeClassD ="active";
    this.activeClassM ="";
    this.activeClass ="";
  }
  CityGraphMonthly() {
    this.chartDB.series = [this.CityGraphicalDataMonthly[0], this.CityGraphicalDataMonthly[1]]
    this.chartDB.xaxis = this.CityGraphicalDataMonthly[2]
    this.isCheck = true;
    this.activeClassD ="";
    this.activeClassM ="active";
    this.activeClass ="";
  }

  CityGraph() {
    this.chartDB.series = [this.CityGraphicalData[0], this.CityGraphicalData[1]]
    this.chartDB.xaxis = this.CityGraphicalData[2]
    this.isCheck = true;
    this.activeClassD ="";
    this.activeClassM ="";
    this.activeClass ="active";
  }
  onSubmitRestaurant() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);
    debugger;
    const val = this.restaurantReportsForm.controls['RestaurantName'];
    let body =
    {
      RestaurantName: this.restaurantReportsForm.controls['RestaurantName'].value,
    }
    debugger;
    this.restaurantService.searchRestaurantReports(body).subscribe(
      data => {
        debugger
        let x_axis: any = []
        let y1_axis: any = []
        let y2_axis: any = []
        this.RestaurantReportsAdd = data['data']['Yearly'];
        for (let i = 0; i < this.RestaurantReportsAdd.length; i++) {
          x_axis.push(this.RestaurantReportsAdd[i].Year)
          y1_axis.push(this.RestaurantReportsAdd[i].TotalAmount)
          y2_axis.push(this.RestaurantReportsAdd[i].SalesTax)
        }
        this.RestaurantGraphicalData = [{
          name: 'Sales Amount',
          data: y1_axis
        }, {
          name: 'Sales Tax',
          data: y2_axis
        }, {
          categories: x_axis,
        }]

        debugger
        let Mx_axis: any = []
        let My1_axis: any = []
        let My2_axis: any = []
        this.RestaurantReportsAddMonthly = data['data']['Monthly'];
        for (let i = 0; i < this.RestaurantReportsAddMonthly.length; i++) {
          Mx_axis.push(this.RestaurantReportsAddMonthly[i].Month)
          My1_axis.push(this.RestaurantReportsAddMonthly[i].TotalAmount)
          My2_axis.push(this.RestaurantReportsAddMonthly[i].SalesTax)
        }
        this.RestaurantGraphicalDataMonthly = [{
          name: 'Sales Amount',
          data: My1_axis
        }, {
          name: 'Sales Tax',
          data: My2_axis
        }, {
          categories: Mx_axis,
        }]

        let Dx_axis: any = []
        let Dy1_axis: any = []
        let Dy2_axis: any = []
        this.RestaurantReportsAddDaily = data['data']['Daily'];
        for (let i = 0; i < this.RestaurantReportsAddDaily.length; i++) {
          Dx_axis.push(this.RestaurantReportsAddDaily[i].Day)
          Dy1_axis.push(this.RestaurantReportsAddDaily[i].TotalAmount)
          Dy2_axis.push(this.RestaurantReportsAddDaily[i].SalesTax)
        }
        console.log(Dx_axis)
        console.log(Dy1_axis)
        console.log(Dy2_axis)
        this.RestaurantGraphicalDataDaily = [{
          name: 'Sales Amount',
          data: Dy1_axis
        }, {
          name: 'Sales Tax',
          data: Dy2_axis
        }, {
          categories: Dx_axis,
        }]

        // this.chartDB.series = [{
        //   name: 'Sales Amount', 
        //   data:  y1_axis
        //   }, {
        //     name: 'Sales Tax',
        //     data:y2_axis
        //   }]       
        // this.chartDB.xaxis = {
        //   categories:x_axis,
        // } 
        console.log("Restaurant report Data: ", this.RestaurantReportsAdd);
        debugger
        this.RestaurantGraphDaily();
      },
      err => {
        debugger;
        console.log("Error  ", err);

      });
    this.isCheck = true;
  }
  RestaurantGraphDaily() {
    this.chartDB.series = [this.RestaurantGraphicalDataDaily[0], this.RestaurantGraphicalDataDaily[1]]
    this.chartDB.xaxis = this.RestaurantGraphicalDataDaily[2]
    this.isCheck = true;
    this.activeClassD ="active";
    this.activeClassM ="";
    this.activeClass ="";
  }
  RestaurantGraphMonthly() {
    this.chartDB.series = [this.RestaurantGraphicalDataMonthly[0], this.RestaurantGraphicalDataMonthly[1]]
    this.chartDB.xaxis = this.RestaurantGraphicalDataMonthly[2]
    this.isCheck = true;
    this.activeClassD ="";
    this.activeClassM ="active";
    this.activeClass ="";
  }

  RestaurantGraph() {
    this.chartDB.series = [this.RestaurantGraphicalData[0], this.RestaurantGraphicalData[1]]
    this.chartDB.xaxis = this.RestaurantGraphicalData[2]
    this.isCheck = true;
    this.activeClassD ="";
    this.activeClassM ="";
    this.activeClass ="active";
  }

  getRegionList() {
    this.restaurantService.getRegions().subscribe(
      data => {
        this.Regions = data['data'];
        console.log("Get Region List: ", this.Regions);
        // Calling the DT trigger to manually render the table
        //  this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getDivisionList() {
    this.restaurantService.getDivision().subscribe(
      data => {
        this.Divisions = data['data'];
        console.log("Get Division List: ", this.Divisions);
        // Calling the DT trigger to manually render the table
        //  this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getDistrictList() {
    this.restaurantService.getDistrict().subscribe(
      data => {
        debugger
        this.Districts = data['data'];
        console.log("Get District List: ", this.Districts);
        // Calling the DT trigger to manually render the table
        // this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getCityList() {
    this.restaurantService.getCity().subscribe(
      data => {
        //console.log(data['data'])

        this.CityList = data['data'];
        console.log("Get City List: ", this.CityList);
        // Calling the DT trigger to manually render the table
        // this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getRestaurantList() {
    //  this.Restaurant = this.restaurantService.signUpForm;
    // this.cancel = this.menuService.cancelD;
    this.restaurantService.AllRestaurants(body).subscribe(
      data => {
        debugger;
        console.log(data);
        this.RestaurantList = data['data'];
        console.log("Restaurant list: ", this.RestaurantList)
        //this.restaurantService.addRestaurantListinStaff(this.RestaurantList);
        debugger;
      },
      err => {
        debugger;
        console.log("Error  ", err);
        // this.error = err.error.data.description;
        // this.loading = false;

      }
    );
    debugger;
  }

  ngOnDestroy() {
    if (this.intervalSub) {
      clearInterval(this.intervalSub);
    }
    if (this.intervalMain) {
      clearInterval(this.intervalMain);
    }
  }
}
function body(body: any) {
  throw new Error('Function not implemented.');
}

