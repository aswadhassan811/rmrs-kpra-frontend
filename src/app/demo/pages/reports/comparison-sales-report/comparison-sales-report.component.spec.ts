import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparisonSalesReportComponent } from './comparison-sales-report.component';

describe('ComparisonSalesReportComponent', () => {
  let component: ComparisonSalesReportComponent;
  let fixture: ComponentFixture<ComparisonSalesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComparisonSalesReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparisonSalesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
