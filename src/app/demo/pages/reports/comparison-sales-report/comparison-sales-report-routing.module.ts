import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComparisonSalesReportComponent } from './comparison-sales-report.component';

const routes: Routes = [
  {
    path: '',
    component: ComparisonSalesReportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComparisonSalesReportRoutingModule { }
