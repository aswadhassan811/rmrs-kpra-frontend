import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatusReportRoutingModule } from './status-report-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StatusReportRoutingModule,
    SharedModule
  ]
})
export class StatusReportModule { }
