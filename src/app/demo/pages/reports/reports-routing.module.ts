import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'sales-report',
        loadChildren: () => import('./sales-report/sales-report.module').then(module => module.SalesReportModule)
      },
      {
        path: 'tax-collection-report',
        loadChildren: () => import('./tax-collection-report/tax-collection-report.module').then(module => module.TaxCollectionReportModule)
      },
      {
        path: 'comparison-sales-report',
        loadChildren: () => import('./comparison-sales-report/comparison-sales-report.module').then(module => module.ComparisonSalesReportModule)
      },
      {
        path: 'suspected-discrepancy-reports',
        loadChildren: () => import('./suspected-discrepancy-reports/suspected-discrepancy-reports.module').then(module => module.SuspectedDiscrepancyReportsModule)
      },
      {
        path: 'status-report',
        loadChildren: () => import('./status-report/status-report.module').then(module => module.StatusReportModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
