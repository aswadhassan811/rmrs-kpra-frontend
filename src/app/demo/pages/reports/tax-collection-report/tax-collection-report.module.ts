import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaxCollectionReportRoutingModule } from './tax-collection-report-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TaxCollectionReportRoutingModule,
    SharedModule
  ]
})
export class TaxCollectionReportModule { }
