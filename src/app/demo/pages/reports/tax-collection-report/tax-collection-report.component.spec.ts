import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxCollectionReportComponent } from './tax-collection-report.component';

describe('TaxCollectionReportComponent', () => {
  let component: TaxCollectionReportComponent;
  let fixture: ComponentFixture<TaxCollectionReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxCollectionReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxCollectionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
