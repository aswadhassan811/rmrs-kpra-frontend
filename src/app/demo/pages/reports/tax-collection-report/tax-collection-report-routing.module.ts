import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaxCollectionReportComponent } from './tax-collection-report.component';

const routes: Routes = [
  {
    path: '',
    component: TaxCollectionReportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxCollectionReportRoutingModule { }
