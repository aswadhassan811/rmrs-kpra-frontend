import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { SalesReportComponent } from './sales-report/sales-report.component';
import { TaxCollectionReportComponent } from './tax-collection-report/tax-collection-report.component';
import { StatusReportComponent } from './status-report/status-report.component';
import { SuspectedDiscrepancyReportsComponent } from './suspected-discrepancy-reports/suspected-discrepancy-reports.component';
import { ComparisonSalesReportComponent } from './comparison-sales-report/comparison-sales-report.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';


@NgModule({
  declarations: [
    SalesReportComponent,
    TaxCollectionReportComponent,
    StatusReportComponent,
    SuspectedDiscrepancyReportsComponent,
    ComparisonSalesReportComponent
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule,
    DataTablesModule,
    NgbTabsetModule,
    NgApexchartsModule
  ]
})
export class ReportsModule { }
