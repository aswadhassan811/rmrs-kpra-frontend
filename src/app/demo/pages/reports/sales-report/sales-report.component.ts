
import { Component, OnInit, ViewChild } from '@angular/core';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.component.html',
  styleUrls: ['./sales-report.component.scss']
})
export class SalesReportComponent implements OnInit {
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  min: number;
  max: number;
  dtOptions: any = {};
  invoices: any;
  dtTrigger: Subject<any> = new Subject<any>();
  RestaurantList: any;
  error: any;
  loading: boolean;
  Regions: any;
  Divisions: any;
  Districts: any;
  saleReportsForm: any;
  SalesReportsAdd: any;
  CityList: any;
  RegionName: string;

  constructor(private formBuilder: FormBuilder, private restaurantService: RestaurantService) { }

  ngOnInit(): void {

    // We need to call the $.fn.dataTable like this because DataTables typings do not have the "ext" property
    $.fn['dataTable'].ext.search.push((settings, data, dataIndex) => {
      const id = parseFloat(data[3] || [4]) || 0; // use data for the id column
      if ((isNaN(this.min) && isNaN(this.max)) ||
        (isNaN(this.min) && id <= this.max) ||
        (this.min <= id && isNaN(this.max)) ||
        (this.min <= id && id <= this.max)) {
        return true;
      }
      return false;
    });

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [1, 'desc'],
      processing: true,
      dom: 'Bfrtip',
      lengthMenu: [10, 20, 50, 100],
      buttons: [
        'print', 'csv', 'pageLength'
      ]
    };
    this.saleReportsForm = this.formBuilder.group({
      RegionName: new FormControl(''),
      DivisionName: new FormControl(''),
      DistrictName: new FormControl(''),
      CityName: new FormControl(''),
      RestaurantName: new FormControl(''),
      StartDate: new FormControl(''),
      EndDate: new FormControl(''),
    });
    this.getRestaurantsInvoices();
    this.getRestaurantList();
    this.getRegionList();
    this.getDivisionList();
    this.getDistrictList();
    this.getCityList();
  }
  getRestaurantsInvoices() {

    this.restaurantService.SalesReports().subscribe(
      res => {
        this.invoices = res["data"];
        console.log("Search sales Data:", res["data"])
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next();
      });

  }
  getRestaurantList() {
    //  this.Restaurant = this.restaurantService.signUpForm;
    // this.cancel = this.menuService.cancelD;
    this.restaurantService.AllRestaurants(body).subscribe(
      data => {
        debugger;
        console.log(data);
        this.RestaurantList = data['data'];
        console.log("Restaurant list: ", this.RestaurantList)
        //this.restaurantService.addRestaurantListinStaff(this.RestaurantList);
        debugger;
      },
      err => {
        debugger;
        console.log("Error  ", err);
        this.error = err.error.data.description;
        this.loading = false;

      }
    );
    debugger;
  }
  getRegionList() {
    this.restaurantService.getRegions().subscribe(
      data => {
        this.Regions = data['data'];
        console.log("Get Region List: ", this.Regions);
        // Calling the DT trigger to manually render the table
        //  this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getDivisionList() {
    this.restaurantService.getDivision().subscribe(
      data => {
        this.Divisions = data['data'];
        console.log("Get Division List: ", this.Divisions);
        // Calling the DT trigger to manually render the table
        //  this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getDistrictList() {
    this.restaurantService.getDistrict().subscribe(
      data => {
        debugger
        this.Districts = data['data'];
        console.log("Get District List: ", this.Districts);
        // Calling the DT trigger to manually render the table
        // this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  getCityList() {
    this.restaurantService.getCity().subscribe(
      data => {
        //console.log(data['data'])
       
        this.CityList = data['data'];
        console.log("Get City List: ", this.CityList);
        // Calling the DT trigger to manually render the table
        // this.dtTrigger.next();
      },
      err => {
      }
    );
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
    $.fn['dataTable'].ext.search.pop();

  }
  onSubmit() {
    // this.cancel = true;
    // this.menuService.cancelF(this.cancel);
    debugger;
    const val = this.saleReportsForm.controls['RegionName'];
    let body =
    {
      RegionName: this.saleReportsForm.controls['RegionName'].value,
      DivisionName: this.saleReportsForm.controls['DivisionName'].value,
      DistrictName: this.saleReportsForm.controls['DistrictName'].value,
      CityName: this.saleReportsForm.controls['CityName'].value,
      RestaurantName: this.saleReportsForm.controls['RestaurantName'].value,
    }
    debugger;
    //console.log(this.RegionID)
    this.restaurantService.searchSalesReports(body).subscribe(
      data => {
        debugger
        this.SalesReportsAdd = data['data'];
        console.log("Sales report Data: ", this.SalesReportsAdd);
        debugger
        //  this.ngOnInit();
        this.saleReportsForm.clear();
      },
      err => {
        debugger;
        console.log("Error  ", err);

      });
  }
  // filterById(): void {
  //   this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //     dtInstance.draw();
  //   });
  // }
  clearSearch(){
    debugger
    this.saleReportsForm.reset();
    //  DivisionName: this.saleReportsForm.controls['DivisionName'].value,
    //  DistrictName: this.saleReportsForm.controls['DistrictName'].value,
    //  CityName: this.saleReportsForm.controls['CityName'].value,
    //  RestaurantName: this.saleReportsForm.controls['RestaurantName'].value,
   
   }
}


function body(body: any) {
  throw new Error('Function not implemented.');
}


