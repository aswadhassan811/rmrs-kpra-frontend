import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/shared/services/menu.service';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.scss']
})
export class StaffListComponent implements OnInit {
  menu: any;
  condition: boolean = false;
  abc: boolean = false;
  cancel: boolean;
  Staff: any
  constructor(private menuService: MenuService, private router: Router) { }

  ngOnInit(): void {
    this.Staff = this.menuService.staff;
    this.condition = this.menuService.foo;
    debugger;
    this.cancel = this.menuService.cancelD;
    debugger;
  }
  addStaff() {
    this.menuService.decision(this.abc);
    this.router.navigate(['/staff/staff-add']);
  }
  editStaff() {
    this.abc = true;
    this.menuService.decision(this.abc);
    debugger;
    let staffMember = {
      staffName: this.menuService.staff.value.Name,
      DOB: this.menuService.staff.value.DOB,
      contactNumber: this.menuService.staff.value.ContactNumber,
      userName: this.menuService.staff.value.UserName,
      cnicNo: this.menuService.staff.value.CNICNo,
      address: this.menuService.staff.value.Address,
      password: this.menuService.staff.value.Password,
      permission: this.menuService.staff.value.Permission,
    }
    debugger;
    this.menuService.preFilledStaff(staffMember);
    this.router.navigate(['/staff/staff-add']);
  }
}
