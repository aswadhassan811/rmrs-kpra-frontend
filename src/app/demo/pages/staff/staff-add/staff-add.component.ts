import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm, ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/shared/services/menu.service';
@Component({
  selector: 'app-staff-add',
  templateUrl: './staff-add.component.html',
  styleUrls: ['./staff-add.component.scss']
})
export class StaffAddComponent implements OnInit {
  addStaffForm: FormGroup;
  cancel: boolean;
  xyz: boolean;
  constructor(private formBuilder: FormBuilder, private menuService: MenuService, private router: Router) { }

  ngOnInit(): void {
    this.addStaffForm = this.formBuilder.group({
      StaffName: new FormControl('', [
        Validators.required,
        Validators.pattern("[A-Za-z].{2,}")]),
      Password: new FormControl('', [
        Validators.required, Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')]),
      CNICNo: new FormControl('', [
        Validators.required,
        Validators.pattern("^[0-9]{13}$")]),
      DOB: new FormControl('', [
        Validators.required]),
      Address: new FormControl('', [
        Validators.required]),
      UserName: new FormControl('', [
        Validators.required,
        Validators.pattern("[A-Za-z].{2,}")]),
      ContactNumber: new FormControl('', [
        Validators.required,
        Validators.pattern(".{10,10}")]),
        // Permission: new FormControl('', [
        // Validators.required]),

    }

    );

    this.menuService.updateStaffList(this.addStaffForm);
    debugger;
    this.addStaffForm.controls['StaffName'].patchValue(this.menuService.staffMember.staffName);
    this.addStaffForm.controls['DOB'].patchValue(this.menuService.staffMember.DOB);
    this.addStaffForm.controls['ContactNumber'].patchValue(this.menuService.staffMember.contactNumber);
    this.addStaffForm.controls['UserName'].patchValue(this.menuService.staffMember.userName);
    this.addStaffForm.controls['CNICNo'].patchValue(this.menuService.staffMember.cnicNo);
    this.addStaffForm.controls['Address'].patchValue(this.menuService.staffMember.address);
    this.addStaffForm.controls['Password'].patchValue(this.menuService.staffMember.password);
    // this.addStaffForm.controls['Permission'].patchValue(this.menuService.staffMember.permission);
    debugger;
    this.xyz = this.menuService.decide;
  }
  onSubmit() {
    this.addStaffForm.markAllAsTouched()
    this.cancel = true;
    this.menuService.cancelF(this.cancel);
    this.router.navigate(['/staff/staff-list']);

  }
  canceled() {
    this.router.navigate(['/staff/staff-list']);
  }
}
