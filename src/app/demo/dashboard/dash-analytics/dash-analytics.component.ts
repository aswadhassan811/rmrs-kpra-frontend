import { Component, OnInit, ViewChild } from '@angular/core';
import { ApexChartService } from '../../../theme/shared/components/chart/apex-chart/apex-chart.service';
import { ChartComponent, ApexAxisChartSeries, ApexChart, ApexXAxis, ApexTitleSubtitle, ApexDataLabels, ApexStroke, ApexTooltip } from "ng-apexcharts";
import { RestaurantService } from 'src/app/shared/services/restaurant.service';
import { FormBuilder, FormControl } from '@angular/forms';


export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  title: ApexTitleSubtitle;
  dataLabels: ApexDataLabels;
  stroke: ApexStroke;
  tooltip: ApexTooltip
};
@Component({
  selector: 'app-dash-analytics',
  templateUrl: './dash-analytics.component.html',
  styleUrls: ['./dash-analytics.component.scss']
})
export class DashAnalyticsComponent implements OnInit {
  @ViewChild("chart", { static: false }) chart: ChartComponent;
  public chartDB: Partial<ChartOptions>;

  public lastDate: number;
  public line2CAC: any;
  public data: any;

  public intervalSub: any;
  public intervalMain: any;

  public dailyVisitorStatus: string;
  public dailyVisitorAxis: any;
  LastYearData: any;
  Regions: any;
  Divisions: any;
  Districts: any;
  CityList: any;
  RestaurantList: any;
  regionReportsForm: any;
  DashboardDataLastYear: any;
  RegionName: string;
  y1_axis: any;
  y2_axis: any;
  RegionReportsAddDaily: any;
  RegionGraphicalDataDaily: any;
  divisionReportsForm: any;
  DivisionReportsAdd: any;
  DivisionGraphicalData: any;
  DivisionReportsAddDaily: any;
  DivisionGraphicalDataDaily: any;
  districtReportsForm: any;
  DistrictReportsAdd: any;
  DistrictGraphicalData: any;
  DistrictReportsAddDaily: any;
  DistrictGraphicalDataDaily: any;
  RegionReportsAddMonthly: any;
  RegionGraphicalDataMonthly: any;
  DivisionReportsAddMonthly: any;
  DivisionGraphicalDataMonthly: any;
  DistrictReportsAddMonthly: any;
  DistrictGraphicalDataMonthly: any;
  cityReportsForm: any;
  CityReportsAdd: any;
  CityGraphicalData: any;
  CityReportsAddMonthly: any;
  CityGraphicalDataMonthly: any;
  CityReportsAddDaily: any;
  CityGraphicalDataDaily: any;
  restaurantReportsForm: any;
  RestaurantReportsAdd: any;
  RestaurantGraphicalData: any;
  RestaurantReportsAddMonthly: any;
  RestaurantGraphicalDataMonthly: any;
  RestaurantReportsAddDaily: any;
  RestaurantGraphicalDataDaily: any;
  isCheck: boolean = false;
  DashboardData: any;
  DashboardDataCurrentYear: any;
  CurrentYearData: any;
  activeClassL: any;
  activeClassC: string;
  CumulativeCurrentYearData:any;
  constructor(public apexEvent: ApexChartService, private formBuilder: FormBuilder, private restaurantService: RestaurantService) {
    this.chartDB = {
      chart: {
        height: 350,
        type: 'area',
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      // series: [{
      //   name: 'Sales',
      //   data: [21, 1232, 43, 23, 32]
      // }, {
      //   name: 'Sales Tax',
      //   data: [21, 12, 43, 23, 32]
      // }],

      // xaxis: {
      //   categories: [2017, 2016, 2018, 2019, 2020, 2021],
      // },
      tooltip: {
        x: {
          format: 'dd/MM/yy HH:mm'
        },
      }
    };
  }


  ngOnInit() {

    // this.regionReportsForm = this.formBuilder.group({
    //   RegionName: new FormControl(''),
    // });
    // this.divisionReportsForm = this.formBuilder.group({
    //   DivisionName: new FormControl(''),
    // });
    // this.districtReportsForm = this.formBuilder.group({
    //   DistrictName: new FormControl(''),
    // });
    // this.cityReportsForm = this.formBuilder.group({
    //   CityName: new FormControl(''),
    // });
    // this.restaurantReportsForm = this.formBuilder.group({
    //   RestaurantName: new FormControl(''),
    // });

    this.getDashboard();
 
  }

  // onSubmit() {
  //   // this.cancel = true;
  //   // this.menuService.cancelF(this.cancel);
  //   debugger;
  //   const val = this.regionReportsForm.controls['RegionName'];
  //   let body =
  //   {
  //     RegionName: this.regionReportsForm.controls['RegionName'].value,
  //   }
  //   debugger;
  //   this.restaurantService.searchRegionReports(body).subscribe(
  //     data => {
  //       debugger
  //       let x_axis: any = []
  //       let y1_axis: any = []
  //       let y2_axis: any = []
  //       this.RegionReportsAdd = data['data']['Yearly'];
  //       for (let i = 0; i < this.RegionReportsAdd.length; i++) {
  //         x_axis.push(this.RegionReportsAdd[i].Year)
  //         y1_axis.push(this.RegionReportsAdd[i].TotalAmount)
  //         y2_axis.push(this.RegionReportsAdd[i].SalesTax)
  //       }
  //       this.RegionGraphicalData = [{
  //         name: 'Sales Amount',
  //         data: y1_axis
  //       }, {
  //         name: 'Sales Tax',
  //         data: y2_axis
  //       }, {
  //         categories: x_axis,
  //       }]

  //       debugger
  //       let Mx_axis: any = []
  //       let My1_axis: any = []
  //       let My2_axis: any = []
  //       this.RegionReportsAddMonthly = data['data']['Monthly'];
  //       for (let i = 0; i < this.RegionReportsAddMonthly.length; i++) {
  //         Mx_axis.push(this.RegionReportsAddMonthly[i].Month)
  //         My1_axis.push(this.RegionReportsAddMonthly[i].TotalAmount)
  //         My2_axis.push(this.RegionReportsAddMonthly[i].SalesTax)
  //       }
  //       this.RegionGraphicalDataMonthly = [{
  //         name: 'Sales Amount',
  //         data: My1_axis
  //       }, {
  //         name: 'Sales Tax',
  //         data: My2_axis
  //       }, {
  //         categories: Mx_axis,
  //       }]

  //       let Dx_axis: any = []
  //       let Dy1_axis: any = []
  //       let Dy2_axis: any = []
  //       this.RegionReportsAddDaily = data['data']['Daily'];
  //       for (let i = 0; i < this.RegionReportsAddDaily.length; i++) {
  //         Dx_axis.push(this.RegionReportsAddDaily[i].Day)
  //         Dy1_axis.push(this.RegionReportsAddDaily[i].TotalAmount)
  //         Dy2_axis.push(this.RegionReportsAddDaily[i].SalesTax)
  //       }
  //       console.log(Dx_axis)
  //       console.log(Dy1_axis)
  //       console.log(Dy2_axis)
  //       this.RegionGraphicalDataDaily = [{
  //         name: 'Sales Amount',
  //         data: Dy1_axis
  //       }, {
  //         name: 'Sales Tax',
  //         data: Dy2_axis
  //       }, {
  //         categories: Dx_axis,
  //       }]
  //       console.log("Region report Data: ", this.RegionReportsAdd);
  //       debugger
  //     },
  //     err => {
  //       debugger;
  //       console.log("Error  ", err);

  //     });

  // }
  // RegionGraphDaily() {
  //   this.chartDB.series = [this.RegionGraphicalDataDaily[0], this.RegionGraphicalDataDaily[1]]
  //   this.chartDB.xaxis = this.RegionGraphicalDataDaily[2]
  // }
  // RegionGraphMonthly() {
  //   this.chartDB.series = [this.RegionGraphicalDataMonthly[0], this.RegionGraphicalDataMonthly[1]]
  //   this.chartDB.xaxis = this.RegionGraphicalDataMonthly[2]
  // }

  // RegionGraph() {
  //   this.chartDB.series = [this.RegionGraphicalData[0], this.RegionGraphicalData[1]]
  //   this.chartDB.xaxis = this.RegionGraphicalData[2]
  // }

  getDashboard() {
    this.restaurantService.dashboardReports().subscribe(
      data => {
        debugger;
        this.DashboardData = data['data']['PreviousYear'];
        console.log("Dashboard Data: ", this.DashboardData);


        let x_axis: any = []
        let y1_axis: any = []
        let y2_axis: any = []
        this.DashboardDataLastYear = data['data']['PreviousYear']['rawData'];
        for (let i = 0; i < this.DashboardDataLastYear.length; i++) {
          x_axis.push(this.DashboardDataLastYear[i].Month)
          y1_axis.push(this.DashboardDataLastYear[i].TotalAmount)
          y2_axis.push(this.DashboardDataLastYear[i].SalesTax)
        }
        this.LastYearData = [{
          name: 'Sales Amount',
          data: y1_axis
        }, {
          name: 'Sales Tax',
          data: y2_axis
        }, {
          categories: x_axis,
        }]
        console.log("Dashboard last year Data: ", this.DashboardDataLastYear);
        this.LastYear();

        debugger
              let Cx_axis: any = []
              let Cy1_axis: any = []
              let Cy2_axis: any = []
              this.DashboardDataCurrentYear = data['data']['ThisYear']['rawData'];
              for (let i = 0; i < this.DashboardDataCurrentYear.length; i++) {
                Cx_axis.push(this.DashboardDataCurrentYear[i].Month)
                Cy1_axis.push(this.DashboardDataCurrentYear[i].TotalAmount)
                Cy2_axis.push(this.DashboardDataCurrentYear[i].SalesTax)
              }
              this.CumulativeCurrentYearData = {
                TotalInvoices : data['data']['ThisYear']['TotalInvoices'],
                TotalRestaurants : data['data']['ThisYear']['TotalRestaurants'],
                TotalSalesTax : data['data']['ThisYear']['TotalSalesTax']
              }
              
              this.CurrentYearData = [{
                name: 'Sales Amount',
                data: Cy1_axis
              }, {
                name: 'Sales Tax',
                data: Cy2_axis
              }, {
                categories: Cx_axis,
              }]
        
      },
      err => {
      }
    );
  }
  LastYear() {
    this.chartDB.series = [this.LastYearData[0], this.LastYearData[1]]
    this.chartDB.xaxis = this.LastYearData[2]
    this.activeClassL= "active";
    this.activeClassC= "";
  }
 CurrentYear() {
    this.chartDB.series = [this.CurrentYearData[0], this.CurrentYearData[1]]
    this.chartDB.xaxis = this.CurrentYearData[2]
    this.activeClassL= "";
    this.activeClassC= "active";
  }
  ngOnDestroy() {
    if (this.intervalSub) {
      clearInterval(this.intervalSub);
    }
    if (this.intervalMain) {
      clearInterval(this.intervalMain);
    }
  }
}
function body(body: any) {
  throw new Error('Function not implemented.');
}