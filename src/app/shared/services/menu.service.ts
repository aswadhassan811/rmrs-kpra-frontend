import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  menu: any;
  staff: any;
  foo: boolean = false;
  Items: any;
  decide: boolean;
  cancelD: boolean;
  staffMember: any;
  restaurantFields: any;
  restaurantInformation: any;
  reservationFields: any;
  POSMenuForm: any;
  constructor() { }
  updateMenuList(addMenuForm: any) {
    debugger;
    this.menu = addMenuForm;
    this.foo = true;
    debugger;
  }
  preFilledMenu(menuItem: any) {
    debugger;
    this.Items = menuItem;
  }
  decision(abc: boolean) {
    this.decide = abc;
  }
  cancelF(cancel: boolean) {
    this.cancelD = cancel;
  }
  updateStaffList(addStaffForm: any) {
    this.staff = addStaffForm;
    this.foo = true;
  }
  preFilledStaff(eachStaff: any) {
    debugger;
    this.staffMember = eachStaff;
  }
  updateRestaurantList(restaurantAddForm: any) {
    this.restaurantFields = restaurantAddForm;
  }
  preFilledRestaurant(restaurantInfo: any){
    this.restaurantInformation= restaurantInfo;
  }
  updateReservationList(reservationForm: any){
    this.reservationFields = reservationForm;
  }
  POSMenu(addMenuForm: any){
    this.POSMenuForm= addMenuForm;
  }
}