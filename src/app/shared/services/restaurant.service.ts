import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  signUpForm: any;
  preFilled: any;
  RestaurantEdit: any;
  restaurantID: any;
  RestrictionValue: any;
  StaffID: any;
  RestaurantListinAddStaff: any;
  addStaffForm: any;
  RestaurantIdForStaff: any;
  StaffEdit: any;
  updateLuckyDrawID: any;
  
  constructor(private http: HttpClient) { }


  verifyEmail(body: any): Observable<any> {
    debugger;
    return this.http.post(environment.apiUrl + '/owner/verify-email', body);
  }

  sendOTP(body: any): Observable<any> {
    return this.http.post(environment.apiUrl + '/owner/send-otp', body);
  }

  //------------------------------ Restaurant ------------------------------------
  updateRestaurantID(RestaurantID: any) {
    this.restaurantID = RestaurantID;

  }
  EditRestaurant(editRestaurant: any) {
    this.RestaurantEdit = editRestaurant;
  }
  addRestaurant(body: any): Observable<any> {
    debugger;
    return this.http.post(environment.apiUrl + '/restaurant', body);
  }
  updateRestaurantList(body: any) {
    this.signUpForm = body;
  }
  preFilledRestaurantEdit(body: any) {
    this.preFilled = body;

  }
  AllRestaurants(body: any) {
    debugger;
    return this.http.get(environment.apiUrl + '/restaurant', body);
  }
  // restaurantView(body: any) {
  //   debugger;
  //   return this.http.get(environment.apiUrl + '/restaurant/detail/?restaurantID=' + body);
  // }
  // RestaurantIDs(body : any){
  //   this.RestaurantIDS= body;
  // }

  addStaff(body: any): Observable<any> {

    return this.http.post(environment.apiUrl + '/staff', body);
  }
  updateStaffID(StaffID: any) {

    this.StaffID = StaffID;

  }
  addRestaurantListinStaff(body: any) {
    this.RestaurantListinAddStaff = body;
  }
  updateStaffList(body: any) {
    this.addStaffForm = body;
  }

  AllStaff(body: any) {

    return this.http.get(environment.apiUrl + '/staff/?restaurantid=' + body);
  }
  RestuarantIDForStaffList(body: any) {
    this.RestaurantIdForStaff = body;
  }
  EditStaff(editStaff: any) {
    this.StaffEdit = editStaff;
  }
  //------------------------------- Category  --------------------------------------

  getCategoryName() {
    debugger;
    return this.http.get(environment.apiUrl + '/restaurant/category');
  }
  addCategory(body: any) {
    debugger;
    return this.http.post(environment.apiUrl + '/restaurant/category', body);
  }


  restrictionValue(body: any) {
    this.RestrictionValue = body;
  }
  AllRestaurantsInvocies() {
    //  debugger;
    return this.http.get(environment.apiUrl + '/invoice');
  }
  SalesReports() {
    //  debugger;
    return this.http.get(environment.apiUrl + '/reports/sales');
  }
  searchSalesReports(body: any) {
    return this.http.get(environment.apiUrl + '/report/?RegionName=' + body.RegionName +
      '&DivisionName=' + body.DivisionName + '&DistrictName=' + body.DistrictName + '&CityName=' + body.CityName + '&RestaurantName=' + body.RestaurantName);
  }
  searchRegionReports(body: any) {
    return this.http.get(environment.apiUrl + '/reports/region-reports/?RegionName=' + body.RegionName);
  }
  searchDivisionReports(body: any) {
    return this.http.get(environment.apiUrl + '/reports/region-reports/?DivisionName=' + body.DivisionName);
  }
  searchDistrictReports(body: any) {
    return this.http.get(environment.apiUrl + '/reports/region-reports/?DistrictName=' + body.DistrictName);
  }
  searchCityReports(body: any) {
    return this.http.get(environment.apiUrl + '/reports/region-reports/?CityName=' + body.CityName);
  }
  searchRestaurantReports(body: any) {
    return this.http.get(environment.apiUrl + '/reports/region-reports/?RestaurantName=' + body.RestaurantName);
  }

  dashboardReports() {
    return this.http.get(environment.apiUrl + '/reports/dashboard');
  }
  addRegions(body: any) {
    //  debugger;
    return this.http.post(environment.apiUrl + '/location/regions', body);
  }
  getRegions() {
    //  debugger;
    return this.http.get(environment.apiUrl + '/location/regions');
  }
  addDivision(body: any) {
    //  debugger;
    return this.http.post(environment.apiUrl + '/location/divisions', body);
  }
  getDivision() {
    //  debugger;
    return this.http.get(environment.apiUrl + '/location/divisions');
  }
  addDistrict(body: any) {
    //  debugger;
    return this.http.post(environment.apiUrl + '/location/Districts', body);
  }
  getDistrict() {
    //  debugger;
    return this.http.get(environment.apiUrl + '/location/Districts');
  }
  addCity(body: any) {
    //  debugger;
    return this.http.post(environment.apiUrl + '/location/Cities', body);
  }
  getCity() {
    //  debugger;
    return this.http.get(environment.apiUrl + '/location/Cities');
  }
  addLocation(body: any) {
    //  debugger;
    return this.http.post(environment.apiUrl + '/location', body);
  }
  getLocation() {
    //  debugger;
    return this.http.get(environment.apiUrl + '/location/details');
  }
  addUploadCSV(body: any) {
    //  debugger;
    return this.http.post(environment.apiUrl + '/location/bulk-add-regions', body);
  }
  // addDivionsCSV(body: any) {
  //   //  debugger;
  //   return this.http.post(environment.apiUrl + '/location/bulk-add-regions', body);
  // }
  postFile(fileToUpload: File) {
    const endpoint = environment.apiUrl +'/location/bulk-add-regions';
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    //formData.append('ImageCaption', caption);
    return this.http.post(endpoint, formData);
  }
   addLuckyDraw(body: any) {
    //  debugger;
    return this.http.post(environment.apiUrl + '/lucky-draw', body);
  }
  getLuckyDraw() {
    //  debugger;
    return this.http.get(environment.apiUrl + '/lucky-draw');
  }
  getLuckyDrawID(luckyDrawID:any) {
    //  debugger;
    return this.http.get(environment.apiUrl + '/lucky-draw', luckyDrawID);
  }
  getLuckyDrawDetail(body: any) {
    //  debugger;
    return this.http.get(environment.apiUrl + '/lucky-draw/details/?LuckyDrawID=' + body);
  }
  getRunLottery(body: any) {
    //  debugger;
    return this.http.get(environment.apiUrl + '/lucky-draw/run-lottery/?LuckyDrawID=' + body);
  }
  getParticipants(body: any) {
    //  debugger;
    return this.http.get(environment.apiUrl + '/lucky-draw/participants/?LuckyDrawID=' + body);
  }
  
}

