import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  customerID: any;
  constructor(private http: HttpClient) { }
  AuthService_Login(credentials: any): Observable<any> {
    debugger;
    return this.http.post(environment.apiUrl + '/user/login', credentials);
  }
  // public getToken() {
  //   return localStorage.getItem('accessToken');
  // }

  // ProfileService(CustomerID: any): Observable<any>{
  //   return this.http.post(environment.apiUrl + '/owner/profile',{CustomerID}
  //   , environment.httpOptions);
  // }
  // getCustomerID(CustomerID){
  //    this.customerID = CustomerID;

  // }
}